﻿using UnityEngine;
using System.Collections;

public class Angel_Float_script : MonoBehaviour {

	public bool leaving = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(!leaving) {
			Vector3 tarPos = Game.Data.Rushell.Root.transform.position;
			this.gameObject.transform.position = Vector3.Lerp(this.gameObject.transform.position, new Vector3(tarPos.x + 0.7f, tarPos.y + 1.7f, tarPos.z),0.5f);
		} else {
			Vector3 pos = this.gameObject.transform.position;
			pos.x -= 0.5f;
			pos.y += 0.5f;
			this.gameObject.transform.position = pos;
		}
		if(this.gameObject.transform.position.x < -30) {
			Destroy(this.gameObject);
		}
	}
}
