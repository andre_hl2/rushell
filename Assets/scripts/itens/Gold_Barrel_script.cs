﻿using UnityEngine;
using System.Collections;

public class Gold_Barrel_script : MonoBehaviour {

	public bool grounded;
	public LayerMask groundLayer;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		grounded = isGrouded();
		if(!grounded) {
			this.gameObject.GetComponent<Rigidbody2D>().AddForce(Physics2D.gravity);
		}
	}

	bool isGrouded() {
		RaycastHit2D hit = Physics2D.Raycast(this.transform.position, new Vector2(0,-1), 0.74f, groundLayer);
		if(hit.collider != null) {
			this.transform.position = new Vector2(this.transform.position.x, hit.point.y + 0.74f);
			this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
			return true;
		} else {
			return false;
		}
	}

	void OnTriggerEnter2D(Collider2D coll) {
		Debug.Log("Colidiu com "+coll.gameObject.tag);
		if(coll.gameObject.tag == "tiro") {
			Stars_Rank_Script.Data.GoldBarrelsExploded++;
			Destroy(this.gameObject);
		} else if(coll.gameObject.tag == "Player") {
			if(Game.Data.Rushell.pounding) {
				Stars_Rank_Script.Data.GoldBarrelsExploded++;
				Destroy(this.gameObject);
			}
		}
	}
}
