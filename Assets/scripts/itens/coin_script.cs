﻿using UnityEngine;
using System.Collections;

public class coin_script : MonoBehaviour {

	public int value = 1;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Vector2.Distance(this.transform.position,Game.Data.Rushell.Root.transform.position) < Game.Data.Rushell.CoinCollectRadius) {
			Vector2 dir = Game.Data.Rushell.Root.transform.position - this.transform.position;
			dir.Normalize();
			Vector2 pos = this.transform.position;
			pos += (dir/2);
			this.transform.position = pos;
		}
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if(coll.gameObject.tag == "Player") {
			Stars_Rank_Script.Data.roundCoins += value;
			if(SoundController.Data != null)
				if(!SoundController.Data.mutedSFX)
					SoundController.Data.AudioSources["coin_sound"].Play();
			Destroy(this.gameObject);
		}
	}
}
