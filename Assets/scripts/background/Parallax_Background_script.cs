﻿using UnityEngine;
using System.Collections;

public class Parallax_Background_script : MonoBehaviour {

	public float HSpeed;
	public float maxRightPosition;
	public GameObject PositionReference;
	public float distanceToReference;
	public float timesToWalk = 2;
	public bool MoveWithoutCam;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(MoveWithoutCam) {
			Vector3 pos = this.gameObject.transform.position;
			pos.x += HSpeed * (Time.deltaTime * (Game.Data.HSpeed/8f));
			if(pos.x > maxRightPosition) {
				pos.x -= (distanceToReference * timesToWalk);
			}
			this.gameObject.transform.position = pos;
		} else {
			if (Game.Data.cameraFollowing && !Game.Data.onBoss) {
				Vector3 pos = this.gameObject.transform.position;
				pos.x += HSpeed * (Time.deltaTime * (Game.Data.HSpeed/8f));
				if(pos.x > maxRightPosition) {
					pos.x -= (distanceToReference * timesToWalk);
				}
				this.gameObject.transform.position = pos;
			}
		}
	}
}
