﻿using UnityEngine;
using System.Collections;

public class background_move_script : MonoBehaviour {

	public float HSpeed;
	public Vector3 add;
	public bool onlyMoveWithCam;

	// Use this for initialization
	void Start () {
		add = new Vector3(HSpeed,0,0);
		add = Quaternion.Euler(0,0,5) * add;
	}
	
	// Update is called once per frame
	void Update () {
		if(onlyMoveWithCam) {
			if(Game.Data.cameraFollowing && !Game.Data.onBoss){
				this.gameObject.transform.position += (add * Time.deltaTime);
			}
		} else {
			this.gameObject.transform.position += (add * Time.deltaTime);
		}
	}
}
