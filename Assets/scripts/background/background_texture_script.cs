﻿using UnityEngine;
using System.Collections;

public class background_texture_script : MonoBehaviour {

	public bool MoveWithCam;
	public float speed;
	public Renderer renderer;
	public Vector2 initialOffset;
	public float moveTime;
	
	// Use this for initialization
	void Start () {
		renderer.material.mainTextureOffset = initialOffset;
	}
	
	// Update is called once per frame
	void Update () {
		if(MoveWithCam) {
			if(Game.Data.cameraFollowing) 
			{
				moveTime += (Time.deltaTime * (Game.Data.HSpeed/8f));
				Vector2 offset = new Vector2( moveTime * speed, 0);
				Vector2 actualOff = renderer.material.mainTextureOffset;
				actualOff += offset;
				renderer.material.mainTextureOffset = offset;
			}
		} else {
			moveTime += Time.deltaTime;
			Vector2 offset = new Vector2( moveTime * speed, 0);
			Vector2 actualOff = renderer.material.mainTextureOffset;
			actualOff += offset;
			renderer.material.mainTextureOffset = offset;
		}
		
	}
}
