﻿using UnityEngine;
using System.Collections;

public class Control_background_level_script : MonoBehaviour {

	public GameObject dia;
	public GameObject dia_chuva_leve;
	public GameObject dia_chuva_pesada;
	public GameObject noite;
	public GameObject noite_chuva_leve;
	public GameObject noite_dhuva_pesada;

	public AudioSource src;

	// Use this for initialization
	void Start () {

		int lastSelected;

		if(PlayerPrefs.HasKey("last_weather_seleted")) {
			lastSelected = PlayerPrefs.GetInt("last_weather_seleted");
		} else {
			lastSelected = 1;
		}

		int res = Random.Range(0,7);

		while(res == lastSelected) {
			res = Random.Range(0,7);
		}

		dia.SetActive(res == 0);
		dia_chuva_leve.SetActive(res == 1);
		dia_chuva_pesada.SetActive(res == 2);
		noite.SetActive(res == 3);
		noite_chuva_leve.SetActive(res == 4);
		noite_dhuva_pesada.SetActive(res == 5);

		if(SoundController.Data != null) {
			SoundController.Data.BMGAudio1Volume = 1;
			switch(res) {
			case 0:
				SoundController.Data.BMGAudio2Volume = 1;
				SoundController.Data.BMGAudio3Volume = 0;
				SoundController.Data.BMGAudio4Volume = 0;
				break;
			case 1:
				SoundController.Data.BMGAudio2Volume = 1;
				SoundController.Data.BMGAudio3Volume = 0;
				SoundController.Data.BMGAudio4Volume = 0.5f;
				break;
			case 2:
				SoundController.Data.BMGAudio2Volume = 1;
				SoundController.Data.BMGAudio3Volume = 0;
				SoundController.Data.BMGAudio4Volume = 1;
				break;
			case 3:
				SoundController.Data.BMGAudio2Volume = 0;
				SoundController.Data.BMGAudio3Volume = 1;
				SoundController.Data.BMGAudio4Volume = 0;
				break;
			case 4:
				SoundController.Data.BMGAudio2Volume = 0;
				SoundController.Data.BMGAudio3Volume = 1;
				SoundController.Data.BMGAudio4Volume = 0.5f;
				break;
			case 5:
				SoundController.Data.BMGAudio2Volume = 0;
				SoundController.Data.BMGAudio3Volume = 1;
				SoundController.Data.BMGAudio4Volume = 1;
				break;
			case 6:
				SoundController.Data.BMGAudio2Volume = 0.5f;
				SoundController.Data.BMGAudio3Volume = 0.5f;
				SoundController.Data.BMGAudio4Volume = 0;
				break;
			}

			if(!SoundController.Data.mutedSFX) {
				if(res == 2 || res == 5)
					SoundController.Data.AudioSources["rain"].Play();
				else
					SoundController.Data.AudioSources["rain"].Pause();
			}
		}

		PlayerPrefs.SetInt("last_weather_seleted", res);
		PlayerPrefs.Save();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void onDestroy() {
		if(src != null) {
			src.Stop();
		}
	}
}
