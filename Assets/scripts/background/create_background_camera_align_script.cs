﻿using UnityEngine;
using System.Collections;

public class create_background_camera_align_script : MonoBehaviour {

	public GameObject[] objectsToCreate;
	public float sizeOfEach;

	public int timesToCreate;
	int alreadyCreated;
	public Vector3 initialPos;

	// Use this for initialization
	void Start () {
		alreadyCreated = 0;
		for(int i=0;i<timesToCreate;i++) {
			for(int j=0;j<objectsToCreate.Length;j++) {
				GameObject clone = (GameObject)Instantiate(objectsToCreate[j],new Vector3(initialPos.x - (alreadyCreated * sizeOfEach), initialPos.y, initialPos.z),Quaternion.identity);
				clone.transform.RotateAround(initialPos,new Vector3(0,0,1),5);
				alreadyCreated++;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
