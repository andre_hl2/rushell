﻿using UnityEngine;
using UnityEngine.UI;

public class muteComponent : MonoBehaviour {

	public bool BMG;
	public Toggle tg;
	public Sprite on;
	public Sprite off;
	Image img;

	// Use this for initialization
	void Start () {
		img = this.gameObject.GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
		if(BMG){
			if(SoundController.Data.mutedBMG)
				img.sprite = on;
			else
				img.sprite = off;
		} else {
			if(SoundController.Data.mutedSFX)
				img.sprite = on;
			else
				img.sprite = off;
		}

	}
}
