﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShowComponents_Options : MonoBehaviour {

	public Image img;
	public Text txt;
	public Button btn;

	// Use this for initialization
	void Start () {
		img = this.gameObject.GetComponent<Image>();
		txt = this.gameObject.GetComponent<Text>();
		btn = this.gameObject.GetComponent<Button>();
	}
	
	// Update is called once per frame
	void Update () {
		if(img != null)
			img.enabled = SFX.Data.config;
		if(txt != null)
			txt.enabled = SFX.Data.config;
		if(btn != null)
			btn.enabled = SFX.Data.config;
	}
}
