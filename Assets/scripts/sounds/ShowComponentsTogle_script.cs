﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShowComponentsTogle_script : MonoBehaviour {

	public Toggle tg;
	public Image img;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		img.enabled = tg.isOn && SFX.Data.config;
	}
}
