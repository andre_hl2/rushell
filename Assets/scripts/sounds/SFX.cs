﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class SFX : MonoBehaviour {

	public static SFX Data;

	public bool config = false;

	public Toggle mutedBMG;
	public Slider BMGVolume;
	
	public Toggle mutedSFX;
	public Slider SFXVolume;

	// Use this for initialization
	void Start () {
		mutedBMG.onValueChanged.AddListener(updateBMGMuted);
		BMGVolume.onValueChanged.AddListener(updateBMGVolume);
		
		mutedSFX.onValueChanged.AddListener(updateSFXMuted);
		SFXVolume.onValueChanged.AddListener(updateSFXVolume);
	}

	void Awake() {
		if(Data == null) {
			Data = this;
		}
	}

	// Update is called once per frame
	void Update () {
	}

	public void ChangeConfig() {
		config = !config;
	}

	public void updateBMGMuted(bool value) {
		if(SoundController.Data != null) {
			SoundController.Data.mutedBMG = !value;
			if(!value) {
				SoundController.Data.BMGAudioSource1.Pause();
				SoundController.Data.BMGAudioSource2.Pause();
				SoundController.Data.BMGAudioSource3.Pause();
				SoundController.Data.BMGAudioSource4.Pause();
				PlayerPrefs.SetInt("mutedBMG",1);
			} else {
				SoundController.Data.BMGAudioSource1.Play();
				SoundController.Data.BMGAudioSource2.Play();
				SoundController.Data.BMGAudioSource3.Play();
				SoundController.Data.BMGAudioSource4.Play();
				PlayerPrefs.SetInt("mutedBMG",0);
			}
			PlayerPrefs.Save();
		}
	}

	public void updateBMGVolume(float value) {
		if(SoundController.Data != null) {
			SoundController.Data.BMGVolume = value;
			PlayerPrefs.SetFloat("BMGVolume",value);
			PlayerPrefs.Save();
		}
		
	}
	
	public void updateSFXMuted(bool value) {
		if(SoundController.Data != null) {
			SoundController.Data.mutedSFX = !value;
			if(!value) {
				PlayerPrefs.SetInt("mutedSFX",1);
			} else {
				PlayerPrefs.SetInt("mutedSFX",0);
			}
			PlayerPrefs.Save();
		}
	}
	
	public void updateSFXVolume(float value) {
		if(SoundController.Data != null) {
			SoundController.Data.SFXVolume = value;
			PlayerPrefs.SetFloat("SFXVolume",value);
			PlayerPrefs.Save();
		}
	}
}
