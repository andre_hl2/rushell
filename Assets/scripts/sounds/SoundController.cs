﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;

public class SoundController : MonoBehaviour {

	public static SoundController Data;

	public Dictionary<string, AudioSource> AudioSources;
	public float SFXVolume;
	public bool mutedSFX;

	public AudioSource BMGAudioSource1;
	public float BMGAudio1Volume = 1;
	public AudioSource BMGAudioSource2;
	public float BMGAudio2Volume = 1;
	public AudioSource BMGAudioSource3;
	public float BMGAudio3Volume = 1;
	public AudioSource BMGAudioSource4;
	public float BMGAudio4Volume = 1;

	public string actualBMG;
	public float BMGVolume;
	public bool mutedBMG;

	public void Awake() {
		if(Data == null) {
		
			DontDestroyOnLoad(this.gameObject);
			Data = this;
			DOTween.Init();

			BMGAudioSource1 = this.gameObject.GetComponent<AudioSource>();

			BMGAudio1Volume = 1;
			BMGAudio2Volume = 1;
			BMGAudio3Volume = 1;
			BMGAudio4Volume = 1;

			AudioSources = new Dictionary<string, AudioSource>();

			if(PlayerPrefs.HasKey("mutedSFX"))
				mutedSFX = (PlayerPrefs.GetInt("mutedSFX") == 1);
			else
				mutedSFX = false;
			if(PlayerPrefs.HasKey("SFXVolume"))
				SFXVolume = (PlayerPrefs.GetFloat("SFXVolume"));
			else
				SFXVolume = 1;

			if(PlayerPrefs.HasKey("mutedBMG"))
				mutedBMG = (PlayerPrefs.GetInt("mutedBMG") == 1);
			else
				mutedBMG = false;
			if(PlayerPrefs.HasKey("BMGVolume"))
				BMGVolume = PlayerPrefs.GetFloat("BMGVolume");
			else
				BMGVolume = 1;


			if(!mutedBMG) {
				BMGAudioSource1.Play();
				BMGAudioSource1.volume = BMGVolume;
			}

			AudioSource[] sources = this.gameObject.GetComponentsInChildren<AudioSource>();
			for(int i=0;i< sources.Length;i++) {
				sources[i].volume = SFXVolume;
				AudioSources[sources[i].gameObject.name] = sources[i];
				DontDestroyOnLoad(sources[i]);
			}

			DontDestroyOnLoad(BMGAudioSource2.gameObject);
			DontDestroyOnLoad(BMGAudioSource3.gameObject);
			DontDestroyOnLoad(BMGAudioSource4.gameObject);
		} else {
			Data = SoundController.Data;
		}
	}

	// Use this for initialization
	void Start () {
		PlayerPrefs.SetInt("specialWeapon",0);
		PlayerPrefs.SetInt("hasSpecialWeapon",0);
		PlayerPrefs.SetInt("pwu_swipe",0);
		PlayerPrefs.SetInt("pwu_double_tap",0);
		PlayerPrefs.SetInt("pwu_tap_shoot",0);
		PlayerPrefs.SetInt("pwu_tap_swipe",0);
		PlayerPrefs.DeleteKey("jaMostrouHelp");
		PlayerPrefs.Save();
	}

	// Update is called once per frame
	void Update () {
		if(Data != null) {
			if(BMGAudioSource1 != null)
				BMGAudioSource1.volume = BMGVolume * BMGAudio1Volume;
			if(BMGAudioSource2 != null)
				BMGAudioSource2.volume = BMGVolume * BMGAudio2Volume;
			if(BMGAudioSource3 != null)
				BMGAudioSource3.volume = BMGVolume * BMGAudio3Volume;
			if(BMGAudioSource4 != null)
				BMGAudioSource4.volume = BMGVolume * BMGAudio4Volume;

			foreach(string key in AudioSources.Keys) {
				if(AudioSources[key] != BMGAudioSource1)
					AudioSources[key].volume = SFXVolume;
			}
		}
	}
}
