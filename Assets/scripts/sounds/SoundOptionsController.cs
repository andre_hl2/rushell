﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SoundOptionsController : MonoBehaviour {

	public Toggle mutedBMG;
	public Slider BMGVolume;

	public Toggle mutedSFX;
	public Slider SFXVolume;

	// Use this for initialization
	void Start () {

		mutedBMG.isOn = !SoundController.Data.mutedBMG;
		mutedSFX.isOn = !SoundController.Data.mutedSFX;
		BMGVolume.value = SoundController.Data.BMGVolume;
		SFXVolume.value = SoundController.Data.SFXVolume;
	}
	
	// Update is called once per frame
	void Update () {
	}

	

}
