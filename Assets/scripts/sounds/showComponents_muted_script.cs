﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class showComponents_muted_script : MonoBehaviour {

	public Image img;

	public bool CheckIfBMG;

	// Use this for initialization
	void Start () {
		img = this.gameObject.GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
		if(img != null && SoundController.Data != null) {
			if(CheckIfBMG)
				img.enabled = !SoundController.Data.mutedBMG && SFX.Data.config;
			else
				img.enabled = !SoundController.Data.mutedSFX && SFX.Data.config;
		}
	}
}
