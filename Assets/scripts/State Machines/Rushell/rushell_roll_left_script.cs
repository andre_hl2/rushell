﻿using UnityEngine;
using System.Collections;

public class rushell_roll_left_script : StateMachineBehaviour {

	public float Boss_HSpeed;
	public float HSpeed;

	public GameObject shadow;
	int number = 0;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		animator.SetBool("Roll_Left",false);
		Game.Data.Rushell.Bazooka.GetComponent<SpriteRenderer>().enabled = false;
		Game.Data.Rushell.Root.GetComponent<BoxCollider2D>().enabled = false;
		Game.Data.Rushell.Root.GetComponent<CircleCollider2D>().enabled = true;
		Game.Data.Rushell.RigidBody.velocity = new Vector2(0,0);

		if(Game.Data.Rushell.PowerUps.pwu_ghost_rool) {
			Color col = Game.Data.Rushell.Root.GetComponent<SpriteRenderer>().material.color;
			col.a = 0.5f;
			Game.Data.Rushell.Root.GetComponent<SpriteRenderer>().material.color = col;
			Game.Data.Rushell.invencible = true;
		}

	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		Vector2 pos = Game.Data.Rushell.Root.transform.position;
		if(Game.Data.onBoss)
			pos.x += Boss_HSpeed * Time.deltaTime;
		else 
			pos.x += HSpeed * Time.deltaTime;
		Game.Data.Rushell.Root.transform.position = pos;
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		Game.Data.Rushell.Bazooka.GetComponent<SpriteRenderer>().enabled = true;
		Game.Data.Rushell.Root.GetComponent<BoxCollider2D>().enabled = true;

		if(Game.Data.Rushell.PowerUps.pwu_ghost_rool) {
			Color col = Game.Data.Rushell.Root.GetComponent<SpriteRenderer>().material.color;
			col.a = 1;
			Game.Data.Rushell.Root.GetComponent<SpriteRenderer>().material.color = col;
			Game.Data.Rushell.invencible = false;
		}
	}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
