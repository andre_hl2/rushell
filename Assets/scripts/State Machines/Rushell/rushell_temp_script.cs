﻿using UnityEngine;
using System.Collections;

public class rushell_temp_script : StateMachineBehaviour {

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		animator.SetBool("Death", false);
		Game.Data.Rushell.dead = true;
		Game.Data.Rushell.Bazooka.GetComponent<Rigidbody2D>().isKinematic = false;
		Game.Data.Rushell.Bazooka.GetComponent<Rigidbody2D>().AddForce( new Vector2(-200 + Random.Range(0,400),Random.Range(500,1000)));
		Game.Data.Rushell.Bazooka.GetComponent<Rigidbody2D>().AddTorque(-1000);
		Game.Data.Rushell.Bazooka.GetComponent<SpriteRenderer>().sortingOrder = 100;
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	//override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	//override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
