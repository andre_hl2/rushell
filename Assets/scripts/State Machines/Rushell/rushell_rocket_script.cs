﻿using UnityEngine;
using System.Collections;

public class rushell_rocket_script : StateMachineBehaviour {

	public float rocketForce;
	public GameObject shoot;

	public AudioClip shootSound;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		animator.SetBool("Rocket",false);
		Game.Data.Rushell.RigidBody.velocity = new Vector2(Game.Data.Rushell.RigidBody.velocity.x, rocketForce);	
		Game.Data.Rushell.Bazooka.transform.rotation = Quaternion.Euler(new Vector3(0,0,270));
		GameObject clone = (GameObject)Instantiate(shoot,Game.Data.Rushell.Root.transform.position,Quaternion.identity);
		AudioSource.PlayClipAtPoint(shootSound,Camera.main.transform.position);
		clone.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-100,-300));
		if(Game.Data.onBoss) {
			clone.GetComponent<MoveWithCam>().enabled = false;
		}
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	//override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	//override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
