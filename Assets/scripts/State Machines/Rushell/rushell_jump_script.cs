﻿using UnityEngine;
using System.Collections;

public class rushell_jump_script : StateMachineBehaviour {

	public float jumpForce;
	public Vector2 tapPosition;
	public float timePressed;
	public float ver_offset;
	public float hor_offset;
	public float timeOnState;
	public bool double_jump;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		animator.SetBool("Jump",false);
		animator.SetFloat("timeJump",0.0f);
		animator.SetBool("Pound",false);
		animator.SetBool("Float",false);
		animator.SetBool("Turbo",false);
		animator.SetBool("Pogo",false);
		animator.SetBool("Rocket",false);
		animator.SetBool("Roll_Left",false);
		animator.SetBool("Roll_Right",false);
		Game.Data.Rushell.pounding = false;
		if(double_jump)
			Game.Data.Rushell.RigidBody.velocity = new Vector2(0,0);
		Game.Data.Rushell.RigidBody.AddForce(new Vector2(0,jumpForce));
		timeOnState = 0;

		if(Input.touchCount > 0) {
			tapPosition = Input.GetTouch(0).position;
		}
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

		timeOnState += Time.deltaTime;

		if(!double_jump) {
			//INPUTS DE TOUCH
			if(Input.touchCount > 0) {
				if(Input.GetTouch(0).position.y < Screen.height * 0.7) {

					timePressed += Time.deltaTime;

					switch(Input.GetTouch(0).phase) {
						
					case TouchPhase.Began:
						tapPosition = Input.GetTouch(0).position;
						timePressed = 0;
						if(tapPosition.x < Screen.width/2) {
							if(Game.Data.Rushell.PowerUps.pwu_double_jump)
								animator.SetBool("Jump",true);
							else if(Game.Data.Rushell.PowerUps.pwu_float)
								animator.SetBool("Float",true);
							else if(Game.Data.Rushell.PowerUps.pwu_turbo)
								animator.SetBool("Turbo",true);
						}
						break;

					case TouchPhase.Moved:
						if(timeOnState < 0.15) {
							if((Input.GetTouch(0).position.x - tapPosition.x) > hor_offset) {
								if(Game.Data.onBoss) {
									if(Game.Data.Rushell.PowerUps.pwu_roll || Game.Data.Rushell.PowerUps.pwu_ghost_rool) {
										animator.SetBool("Roll_Right",true);
										Game.Data.Rushell.RigidBody.velocity = new Vector2(0,0);
									}
								}
							}
							if((tapPosition.x - Input.GetTouch(0).position.x) > hor_offset) {
								if(Game.Data.Rushell.PowerUps.pwu_roll || Game.Data.Rushell.PowerUps.pwu_ghost_rool) {
									animator.SetBool("Roll_Left",true);
								}
								Game.Data.Rushell.RigidBody.velocity = new Vector2(0,0);
							}
						}
						break;
		
					case TouchPhase.Ended:
						
						if((tapPosition.y - Input.GetTouch(0).position.y) > ver_offset && timePressed < 1.0f) {
							if(Game.Data.Rushell.PowerUps.pwu_pound || Game.Data.Rushell.PowerUps.pwu_mega_pound)
								animator.SetBool("Pound",true);
							else if(Game.Data.Rushell.PowerUps.pwu_pogo)
								animator.SetBool("Pogo",true);
							
						} else if(timePressed < 0.2f) {
							if(Input.GetTouch(0).position.x > Screen.width/2) {
								if(Game.Data.Rushell.PowerUps.pwu_rocket || Game.Data.Rushell.PowerUps.pwu_mega_rocket)
									animator.SetBool("Rocket",true);
								else if(Game.Data.Rushell.PowerUps.pwu_hammer)
									animator.SetBool("Hammer",true);
							}
						}
						timePressed = 0;
						break;
					}
				}
			}

			if(Input.GetKeyDown(KeyCode.Space)) {
				if(Game.Data.Rushell.PowerUps.pwu_double_jump)
					animator.SetBool("Jump",true);
			}

			if(Input.GetKeyDown(KeyCode.Z)) {
				if(Game.Data.Rushell.PowerUps.pwu_rocket || Game.Data.Rushell.PowerUps.pwu_mega_rocket)
					animator.SetBool("Rocket",true);
			}

			if(Input.GetKeyDown(KeyCode.DownArrow)) {
				if(Game.Data.Rushell.PowerUps.pwu_pound || Game.Data.Rushell.PowerUps.pwu_mega_pound)
					animator.SetBool("Pound",true);
			}
			if(Input.GetKeyDown(KeyCode.UpArrow)) {
				if(Game.Data.Rushell.PowerUps.pwu_float)
					animator.SetBool("Float",true);
			}
			if(Input.GetKeyDown(KeyCode.X)) {
				if(Game.Data.Rushell.PowerUps.pwu_turbo)
					animator.SetBool("Turbo",true); 
			}
			if(Input.GetKeyDown(KeyCode.C)) {
				if(Game.Data.Rushell.PowerUps.pwu_pogo)
					animator.SetBool("Pogo",true);
			}
		}
		animator.SetFloat("timeJump",animator.GetFloat("timeJump") + Time.deltaTime);
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	//override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
