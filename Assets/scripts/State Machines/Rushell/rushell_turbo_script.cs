﻿using UnityEngine;
using System.Collections;

public class rushell_turbo_script : StateMachineBehaviour {

	public float HSpeed;

	public GameObject shadow;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		Game.Data.Rushell.floating = true;
		Game.Data.Rushell.RigidBody.velocity = new Vector2(0,0);
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		Game.Data.Rushell.RigidBody.velocity = new Vector2(0,0);

		Vector3 pos = Game.Data.Rushell.Root.transform.position;
		pos.x -= HSpeed * Time.deltaTime;
		Game.Data.Rushell.Root.transform.position = pos;

		Instantiate(shadow, Game.Data.Rushell.Root.transform.position, Quaternion.identity);
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		Game.Data.Rushell.floating = false;
	}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
