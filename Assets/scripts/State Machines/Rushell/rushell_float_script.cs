﻿using UnityEngine;
using System.Collections;

public class rushell_float_script : StateMachineBehaviour {

	public float fallSpeed;
	public GameObject model_angel;
	GameObject angel_float;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		Game.Data.Rushell.floating = true;	
		Game.Data.Rushell.RigidBody.velocity = new Vector2(0,0);
		angel_float = (GameObject)Instantiate(model_angel,new Vector3(15,15,0),Quaternion.identity);
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		Game.Data.Rushell.RigidBody.velocity = new Vector2(0,0);
		Vector3 pos = Game.Data.Rushell.Root.transform.position;
		pos.y -= fallSpeed;
		Game.Data.Rushell.Root.transform.position = pos;

		animator.SetBool("Float", animator.GetInteger("Grounded") == 0);

		if(Input.touchCount > 0) {
			for(int i=0;i<Input.touchCount;i++) {
				if(Input.GetTouch(i).position.x  < Screen.width/2) {
					if(Input.GetTouch(i).phase == TouchPhase.Ended)
						animator.SetBool("Float",false);
				}
			}
		}
		if(Input.GetKeyUp(KeyCode.Space)) {
			animator.SetBool("Float",false);
		}
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		angel_float.GetComponent<Angel_Float_script>().leaving = true;
		Game.Data.Rushell.floating = true;	
	}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
