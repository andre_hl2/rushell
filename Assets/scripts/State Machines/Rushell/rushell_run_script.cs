﻿
using UnityEngine;
using System.Collections;

public class rushell_run_script : StateMachineBehaviour {

	public float addAngle;
	public float angleSpeed;

	public float timePressed;
	public bool rightPressed;
	public Vector2 tapPosition;

	public float hor_offset;

	public AudioClip shootSound;

	// OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		Game.Data.Rushell.Bazooka.transform.rotation = Quaternion.Euler(0,0,-10);
		Game.Data.HSpeed = 8f;
		Game.Data.Rushell.Root.GetComponent<CircleCollider2D>().enabled = false;
		Game.Data.Rushell.Root.GetComponent<BoxCollider2D>().enabled = true;
		Game.Data.canSpecial = true;

		if(Input.touchCount > 0) {
			tapPosition = Input.GetTouch(0).position;
			if(Input.GetTouch(0).position.x > Screen.width/2) {
				rightPressed = true;
			}
		}
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

		//INPUTS DE TOUCH
		if(Input.touchCount > 0) {
			if(Input.GetTouch(0).position.y < Screen.height * 0.7) {
				switch(Input.GetTouch(0).phase) {
				case TouchPhase.Began:
					timePressed = 0;
					tapPosition = Input.GetTouch(0).position;
					if(tapPosition.x > Screen.width/2) {
						if(Game.Data.canShoot) {
							rightPressed = true;
							Game.Data.shooting = true;
							Game.Data.normal_shooting = true;
							Game.Data.angle = 150;
							angleSpeed = -3f;
						}
					} else {
						if(animator.GetInteger("Grounded") > 0)
							animator.SetBool("Jump",true);
					}
					break;
				case TouchPhase.Moved:
					if(Input.GetTouch(0).position.x < Screen.width/2) {
						Game.Data.shooting = false;
						Game.Data.normal_shooting = false;
						rightPressed = false;
					} 
					if(rightPressed) {
						if(Vector2.Distance(tapPosition, Input.GetTouch(0).position) > 10) {
							rightPressed = false;
							Game.Data.shooting = false;
							Game.Data.normal_shooting = false;
						}
					}
					break;
				case TouchPhase.Ended:
					if(Game.Data.shooting) {
						if(Game.Data.normal_shooting) {
							Game.spawnShoot(Game.Data.normal_shoot,Game.Data.Rushell.Bazooka.transform.position);
							if(Game.Data.canShoot){
								Game.Data.canShoot = false;
								Game.Data.actualTimeToShoot = Game.Data.timeToShoot;
							}
							if(SoundController.Data != null)
								if(!SoundController.Data.mutedSFX)
									SoundController.Data.AudioSources["bazooka_shoot"].Play();
						} else if(Game.Data.special_shooting) {
							Game.Data.Rushell.PowerUps.shootSpecial();
							timePressed = 5.0f;
							Game.Data.shooting = false;
							Game.Data.special_shooting = false;
						}
						Game.Data.normal_shooting = false;
						Game.Data.shooting = false;

					} else if((tapPosition.x - Input.GetTouch(0).position.x) > hor_offset && timePressed < 2.0f) {
						Game.Data.shooting = false;
						Game.Data.normal_shooting = false;
						rightPressed = false;
						if(animator.GetInteger("Grounded") > 0 && !Game.Data.passedLevel) {
							animator.SetBool("Roll_Left",true);
						}

					} else if((Input.GetTouch(0).position.x - tapPosition.x) > hor_offset && timePressed < 2.0f && Game.Data.onBoss){
						Game.Data.shooting = false;
						Game.Data.normal_shooting = false;
						rightPressed = false;
						if(animator.GetInteger("Grounded") > 0 && !Game.Data.passedLevel) {
							animator.SetBool("Roll_Right",true);
						}

					}

					rightPressed = false;
					timePressed = 0;
					break;
				}
			}


			if(Input.touchCount > 1) {
				switch(Input.GetTouch(1).phase) {
				case TouchPhase.Began:
					if(Input.GetTouch(1).position.x < Screen.width/2) {
						if(Game.Data.normal_shooting && Game.Data.shooting) {
							Game.Data.normal_shooting = false;
							Game.Data.special_shooting = true;
						}
					}
					break;
				case TouchPhase.Ended:
					if(Game.Data.special_shooting){
						timePressed = 5.0f;
						Game.Data.special_shooting = false;
						Game.Data.shooting = false;
						Game.Data.Rushell.PowerUps.shootSpecial();
					}
					break;
				}
			}
				
		}


		//INPUTS DE TECLADO
		if(Input.GetKeyDown(KeyCode.Space)) {
			if(Game.Data.Rushell.canJump && !Game.Data.passedLevel) {
				animator.SetBool("Jump",true);
			}
		}

		if(Input.GetKeyDown(KeyCode.LeftArrow)) {
			if(!Game.Data.passedLevel) {
				animator.SetBool("Roll_Left",true);
			}
		}
		if(Input.GetKeyDown(KeyCode.RightArrow)) {
			if(!Game.Data.passedLevel && Game.Data.onBoss) {
				animator.SetBool("Roll_Right",true);
			}
		}

		if(Input.GetKeyDown(KeyCode.Z)) {
			if(Game.Data.canShoot) {
				Game.Data.shooting = true;
				Game.Data.normal_shooting = true;
				Game.Data.angle = 150;
				angleSpeed = -3;
			}

		}
		if(Input.GetKeyUp(KeyCode.Z)) {
			if(Game.Data.normal_shooting) {
				Game.spawnShoot(Game.Data.normal_shoot,Game.Data.Rushell.Bazooka.transform.position);
				if(SoundController.Data != null)
					if(!SoundController.Data.mutedSFX)
						SoundController.Data.AudioSources["bazooka_shoot"].Play();
				Game.Data.canShoot = false;
				Game.Data.actualTimeToShoot = Game.Data.timeToShoot;
			}
			Game.Data.normal_shooting = false;
			Game.Data.shooting = false;
		}


		if( (Game.Data.shooting && Game.Data.normal_shooting) || (Game.Data.shooting && Game.Data.special_shooting) ) {
			Game.Data.Rushell.Bazooka.transform.rotation = Quaternion.Euler(0,0,Game.Data.angle+180);

			angleSpeed += addAngle;

			if(Game.Data.angle > 150) {
				Game.Data.angle = 150;
				angleSpeed = -3f;
			}
			if(Game.Data.angle < 110) {
				angleSpeed = 0;
				Game.Data.angle = 110.01f;
			}

			Game.Data.angle += angleSpeed;
		}



	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		Game.Data.shooting = false;	
		Game.Data.canSpecial = false;
	}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
