﻿using UnityEngine;
using System.Collections;

public class rushell_pound_spin_script : StateMachineBehaviour {

	public float stompForce;
	Vector3 initialPos;
	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		animator.SetBool("Pound",false);
		//Game.Data.cameraFollowing = false;
		Game.Data.HSpeed = 6f;
		initialPos = Game.Data.Rushell.Root.transform.position;
		Game.Data.Rushell.Bazooka.GetComponent<SpriteRenderer>().enabled = false;
		Game.Data.Rushell.pounding = true;
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		Game.Data.Rushell.RigidBody.velocity = new Vector2(0,0);
		Game.Data.Rushell.Root.transform.position = initialPos;
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		Game.Data.Rushell.RigidBody.AddForce(new Vector2(0,stompForce));
		Game.Data.Rushell.Bazooka.GetComponent<SpriteRenderer>().enabled = true;
	}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
