﻿using UnityEngine;
using System.Collections;

public class unicorn_summon_giant_script : StateMachineBehaviour {

	public float TimeToSummon;
	float actualTimeToSummon;

	public GameObject giant_boss;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		actualTimeToSummon = TimeToSummon;
		Vector3 pos = animator.gameObject.transform.position;
		pos.x = Random.Range(-7.9f,-0.1f);
		animator.gameObject.transform.position = pos;
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		actualTimeToSummon -= Time.deltaTime;
		if(actualTimeToSummon < 0)
			animator.SetInteger("Attack_Type",0);
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		Instantiate(giant_boss,new Vector3(-15.3f,3.3f,0),Quaternion.identity);
	}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
