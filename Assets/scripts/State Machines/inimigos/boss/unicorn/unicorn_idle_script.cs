﻿using UnityEngine;
using System.Collections;

public class unicorn_idle_script : StateMachineBehaviour {

	public float timeToAttack;
	float actualTimeToAttack;
	public int lastAttack;

	public bool selected;
	public Vector3 target;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		selected = false;
		if(animator.GetInteger("TimesToAttack") != 0){
			switch(animator.GetInteger("Last_Attack")){
			case 1:
				animator.SetInteger("Attack_Type",1);
				animator.SetInteger("TimesToAttack",animator.GetInteger("TimesToAttack") - 1);
				break;
			case 2:
				animator.SetInteger("Attack_Type",2);
				animator.SetInteger("TimesToAttack",animator.GetInteger("TimesToAttack") - 1);
				break;
			case 3:
				animator.SetInteger("Attack_Type",3);
				animator.SetInteger("TimesToAttack",animator.GetInteger("TimesToAttack") - 1);
				break;
			case 4:
				animator.SetInteger("Attack_Type",4);
				animator.SetInteger("TimesToAttack",animator.GetInteger("TimesToAttack") - 1);
				break;
			}
		} else {
			actualTimeToAttack = timeToAttack;
		}
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		if(!selected) {
			actualTimeToAttack -= Time.deltaTime;
			animator.gameObject.transform.position = Vector3.Lerp(animator.gameObject.transform.position,new Vector3(-2,8,-1),0.3f);
			animator.gameObject.GetComponent<BoxCollider2D>().enabled = (Vector3.Distance(animator.gameObject.transform.position,new Vector3(-2,8,-1)) < 0.1f);
			if(actualTimeToAttack < 0) {
				int ran = Random.Range(0,4) + 1;
				while(ran == animator.GetInteger("Last_Attack") || ran == 3){
					ran = Random.Range(0,4) + 1;
				}
				switch(ran) {
				case 1:
					target = new Vector3(-25,4.1f,-1);
					animator.gameObject.GetComponent<unicorn_script>().trail.enabled = false;
					selected = true;
					break;
				case 2:
					animator.SetInteger("Attack_Type",2);
					switch(Game.Data.actualLevel) {
					case 5:
						animator.SetInteger("TimesToAttack",1);
						break;
					case 10:
						animator.SetInteger("TimesToAttack",3);
						break;
					case 15:
						animator.SetInteger("TimesToAttack",6);
						break;
					}
					break;
				case 3:
					animator.SetInteger("Attack_Type",3);
					switch(Game.Data.actualLevel) {
					case 5:
						animator.SetInteger("TimesToAttack",1);
						break;
					case 10:
						animator.SetInteger("TimesToAttack",3);
						break;
					case 15:
						animator.SetInteger("TimesToAttack",5);
						break;
					}
					break;
				case 4:
					animator.SetInteger("Attack_Type",4);
					switch(Game.Data.actualLevel) {
					case 5:
						animator.SetInteger("TimesToAttack",1);
						break;
					case 10:
						animator.SetInteger("TimesToAttack",2);
						break;
					case 15:
						animator.SetInteger("TimesToAttack",4);
						break;
					}
					break;
				}
			}
		} else {
			animator.gameObject.transform.position = Vector3.Lerp(animator.gameObject.transform.position,target,0.3f);
			float distance = Vector3.Distance(animator.gameObject.transform.position,target);
			if(distance < 0.01f) {
				animator.gameObject.GetComponent<unicorn_script>().trail.enabled = true;
				animator.SetInteger("Attack_Type",1);
				if(Game.Data.actualLevel == 10)
					animator.SetInteger("TimesToAttack",3);
				else if(Game.Data.actualLevel == 15)
					animator.SetInteger("TimesToAttack",5);
			}
		}

	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		animator.SetInteger("Last_Attack", animator.GetInteger("Attack_Type"));
	}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
