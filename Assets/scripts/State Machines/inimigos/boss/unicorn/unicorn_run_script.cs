﻿using UnityEngine;
using System.Collections;

public class unicorn_run_script : StateMachineBehaviour {

	public Vector3 initialPos;
	public float Hspeed;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		Vector3 pos = animator.gameObject.transform.position;
		if(Game.Data.actualLevel == 10)
			pos.x += Hspeed*1.1f * Time.deltaTime;
		else if (Game.Data.actualLevel == 15)
			pos.x += Hspeed*1.3f * Time.deltaTime;
		else
			pos.x += Hspeed * Time.deltaTime;
		animator.gameObject.transform.position = pos;
		if(pos.x > 14) {
			animator.SetInteger("Attack_Type",0);
		}
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
