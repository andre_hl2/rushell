﻿using UnityEngine;
using System.Collections;

public class unicorn_stomp_down_script : StateMachineBehaviour {

	public float fallSpeed;
	public LayerMask checkLayer;
	public GameObject shock_wave_right;
	public GameObject shock_wave_left;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		animator.SetBool("Grounded",false);	
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		animator.SetBool("Grounded",false);
		Vector3 pos = animator.gameObject.transform.position;
		pos.y -= fallSpeed * Time.deltaTime;
		animator.gameObject.transform.position = pos;

		float distCheck = 1.21f;
		Vector3 origin = animator.gameObject.transform.position;
		origin.z = 0;
		RaycastHit2D hit = Physics2D.Raycast(origin,new Vector2(0,-1),distCheck,checkLayer);
		if(hit.collider != null) {
			animator.SetBool("Grounded",true);
		}
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		Instantiate(shock_wave_left, new Vector3(animator.gameObject.transform.position.x, 3.6f,0),Quaternion.identity);
		Instantiate(shock_wave_right, new Vector3(animator.gameObject.transform.position.x, 3.6f,0),Quaternion.identity);
	}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
