﻿using UnityEngine;
using System.Collections;

public class unicorn_prepare_stomp_script : StateMachineBehaviour {

	public float timeToFall;
	float actualTimeToFall;
	Vector3 target;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		actualTimeToFall = timeToFall;
		target = new Vector3(Random.Range(-8.2f,-0.2f), 12, -1);
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		animator.gameObject.transform.position = Vector3.Lerp(animator.gameObject.transform.position,target,0.1f);	
		actualTimeToFall -= Time.deltaTime;
		if(actualTimeToFall < 0)
			animator.SetInteger("Attack_Type",0);
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	//override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
