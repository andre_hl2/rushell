﻿using UnityEngine;
using System.Collections;

public class temple_boss_prepare_fall_Script : StateMachineBehaviour {

	public float timeToFall;
	public float actualTimeToFall;
	public float VSpeed;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		animator.SetBool("Down", false);
		animator.gameObject.transform.position = new Vector2(Random.Range(-4.55f, 1.33f), 19);	
		actualTimeToFall = timeToFall;
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		actualTimeToFall -= Time.deltaTime;
		if(actualTimeToFall > 0) {
			animator.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
			Vector2 pos = animator.gameObject.transform.position;
			pos.y -= 0.05f;
			animator.gameObject.transform.position = pos;
		} else {
			animator.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0,VSpeed);
			animator.SetBool("Down",true);
		}
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	//override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
