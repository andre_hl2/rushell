﻿using UnityEngine;
using System.Collections;

public class temple_boss_clap_ground_script : StateMachineBehaviour {

	
	public int numberOfSpawns;
	public float timebetweenSpawns;
	public float timeToSpawn;
	public int alreadySpawned;
	
	public GameObject rock_fall;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		timeToSpawn = timebetweenSpawns;
		alreadySpawned = 0;
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		timeToSpawn -= Time.deltaTime;
		if(timeToSpawn < 0) {
			float ran = Random.Range(-3.58f, 2.98f);
			Instantiate(rock_fall,new Vector3(ran,19,0), Quaternion.identity);
			alreadySpawned++;
			if(alreadySpawned == numberOfSpawns) {
				animator.SetInteger("Attack_Type",0);
			} else {
				timeToSpawn = timebetweenSpawns;
			}
		}	
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	//override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
