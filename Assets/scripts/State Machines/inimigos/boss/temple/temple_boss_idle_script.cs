﻿using UnityEngine;
using System.Collections;

public class temple_boss_idle_script : StateMachineBehaviour {

	public float timeToAttack;
	public float minTimeToAttack;
	public float maxTimeToAttack;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		animator.SetInteger("Attack_Type",0);
		if(animator.GetInteger("TimesToAttack") != 0){
			switch(animator.GetInteger("Last_Type")){
			case 1:
				animator.SetInteger("Attack_Type",1);
				animator.SetInteger("TimesToAttack",animator.GetInteger("TimesToAttack") - 1);
				break;
			case 2:
				animator.SetInteger("Attack_Type",2);
				animator.SetInteger("TimesToAttack",animator.GetInteger("TimesToAttack") - 1);
				break;
			case 3:
				animator.SetInteger("Attack_Type",3);
				animator.SetInteger("TimesToAttack",animator.GetInteger("TimesToAttack") - 1);
				break;
			case 4:
				animator.SetInteger("Attack_Type",4);
				animator.SetInteger("TimesToAttack",animator.GetInteger("TimesToAttack") - 1);
				break;
			}
		} else {
			timeToAttack = Random.Range(minTimeToAttack,maxTimeToAttack);
		}
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {


		timeToAttack -= Time.deltaTime;
		if(timeToAttack < 0) {
			int last = animator.GetInteger("Last_Type");
			int ran = last;

			while(ran == last) {
				ran = Random.Range(1,5);
				if(!TempleBossGame.Data.canRock && ran == 3) {
					ran = last;
				}
			}

			switch(ran) {
			case 1:
				animator.SetInteger("Attack_Type",1);
				switch(Game.Data.actualLevel) {
				case 5:
					animator.SetInteger("TimesToAttack",3);
					break;
				case 10:
					animator.SetInteger("TimesToAttack",5);
					break;
				case 15:
					animator.SetInteger("TimesToAttack",7);
					break;
				}
				break;
			case 2:
				animator.SetInteger("Attack_Type",2);
				switch(Game.Data.actualLevel) {
				case 5:
					animator.SetInteger("TimesToAttack",2);
					break;
				case 10:
					animator.SetInteger("TimesToAttack",4);
					break;
				case 15:
					animator.SetInteger("TimesToAttack",7);
					break;
				}
				break;
			case 3:
				animator.SetInteger("Attack_Type",3);
				switch(Game.Data.actualLevel) {
				case 5:
					animator.SetInteger("TimesToAttack",0);
					break;
				case 10:
					animator.SetInteger("TimesToAttack",1);
					break;
				case 15:
					animator.SetInteger("TimesToAttack",2);
					break;
				}
				break;
			case 4:
				animator.SetInteger("Attack_Type",4);
				animator.SetInteger("TimesToAttack",0);
				break;
			}
		}
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		animator.SetInteger("Last_Type", animator.GetInteger("Attack_Type"));	
	}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
