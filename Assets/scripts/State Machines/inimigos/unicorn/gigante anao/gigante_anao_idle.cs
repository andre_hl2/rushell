﻿using UnityEngine;
using System.Collections;

public class gigante_anao_idle : StateMachineBehaviour {

	public float walkvisionDistance;
	public float walkSpeed;
	public float attackVisionDistace;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		animator.SetBool("Attack",false);	
		animator.gameObject.GetComponent<gigante_script>().attacking = false;
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		if((Game.Data.Rushell.Root.transform.position.x - animator.gameObject.transform.position.x) < walkvisionDistance){
			Vector3 pos = animator.gameObject.transform.position;
			pos.x += walkSpeed * Time.deltaTime;
			animator.gameObject.transform.position = pos;

			if((Game.Data.Rushell.Root.transform.position.x - animator.gameObject.transform.position.x) < attackVisionDistace) {
				animator.SetBool("Attack", true);
			}
		}
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	//override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
