﻿using UnityEngine;
using UnityEngine.UI;

public class ImageGrey_script : MonoBehaviour {

	Image img;

	// Use this for initialization
	void Start () {
		img = this.gameObject.GetComponent<Image>();
		if(img != null) {
			Color col = img.color;
			float media = (col.r + col.g + col.b)/3;
			img.color = new Color(media, media, media, col.a);
		}
	}
	
	// Update is called once per frame
	void Update () {

	}
}
