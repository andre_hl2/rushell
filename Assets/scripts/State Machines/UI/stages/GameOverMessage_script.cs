﻿using UnityEngine;
using UnityEngine.UI;

public class GameOverMessage_script : MonoBehaviour {

	Text txt;

	// Use this for initialization
	void Start () {
		txt = this.gameObject.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		if(txt != null){
			if(Game.Data.Rushell.dead) {
				txt.enabled = true;
				txt.text = Game.Data.deadText;
			} else {
				txt.enabled = false;
			}
		}
	}
}
