﻿using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class btn_Game_Over_Script : MonoBehaviour {

	BlurOptimized blur;
	Grayscale     gray;

	// Use this for initialization
	void Start () {
		blur = Camera.main.GetComponent<BlurOptimized>();
		gray = Camera.main.GetComponent<Grayscale>();
	}
	
	// Update is called once per frame
	void Update () {
		if(Tutorial.Data != null) {
			if(blur != null) {
				blur.enabled = Game.Data.levelTerminated || Tutorial.Data.showing || Game.Data.Paused || Game.Data.Rushell.dead;
			}
		} else {
			if(blur != null) {
				blur.enabled = Game.Data.levelTerminated || Game.Data.Paused || Game.Data.Rushell.dead;
				if(gray != null)
					gray.enabled = Game.Data.Rushell.dead;
			}
		}
	}

	public void GoToNext() {
		PlayerPrefs.SetInt("numberOfLevel",Game.Data.actualLevel + 1);
		if(Game.Data.actualLevel == 4 || Game.Data.actualLevel == 9) {

			if(Game.Data.NameOFLevel == "cena_temple") {
				Application.LoadLevel("cena_boss_temple");
			} else if(Game.Data.NameOFLevel == "cena_unicorn") {
				Application.LoadLevel("cena_boss_unicorn");
			} else if(Game.Data.NameOFLevel == "cena_canabalt") {
				Application.LoadLevel("cena_boss_canabalt");
			}

		} else if(Game.Data.actualLevel == 15) {
			Application.LoadLevel("cena_levelSelect");
		} else  if (Game.Data.actualLevel == 5 || Game.Data.actualLevel == 10) {

			if(Game.Data.NameOFLevel == "cena_temple") {
				Application.LoadLevel("cena_temple");
			} else if(Game.Data.NameOFLevel == "cena_unicorn") {
				Application.LoadLevel("cena_unicorn");
			} else if(Game.Data.NameOFLevel == "cena_canabalt") {
				Application.LoadLevel("cena_canabalt");
			}

		} else {
			Application.LoadLevel(Game.Data.NameOFLevel);
		}
	}

	public void Restart() {
		Game.Data.Rushell.coins += Stars_Rank_Script.Data.roundCoins;
		PlayerPrefs.SetInt("coins", Game.Data.Rushell.coins);
		PlayerPrefs.Save();

		Application.LoadLevel(Application.loadedLevel);
	}

	public void gotoLevelSelect() {
		Game.Data.Rushell.coins += Stars_Rank_Script.Data.roundCoins;
		PlayerPrefs.SetInt("coins", Game.Data.Rushell.coins);
		PlayerPrefs.Save();

		Application.LoadLevel("cena_levelSelect");
	}

	public void FinishedRestart() {
		string key = Game.Data.NameOFLevel.ToString()+"_stars_"+Game.Data.actualLevel.ToString();
		Debug.Log(key);

		int stars = Stars_Rank_Script.Data.getStars();
		if(PlayerPrefs.HasKey(key)) {
			if(PlayerPrefs.GetInt(key) < stars) {
				PlayerPrefs.SetInt(key, stars);
			}
		} else {
			PlayerPrefs.SetInt(key, stars);
		}

		Game.Data.Rushell.coins += Stars_Rank_Script.Data.roundCoins;
		PlayerPrefs.SetInt("coins", Game.Data.Rushell.coins);
		PlayerPrefs.Save();
		
		Application.LoadLevel(Application.loadedLevel);
	}

	public void FinishedGoToSelect() {
		string key = Game.Data.NameOFLevel.ToString()+"_stars_"+Game.Data.actualLevel.ToString();
		Debug.Log(key);
		
		int stars = Stars_Rank_Script.Data.getStars();
		if(PlayerPrefs.HasKey(key)) {
			if(PlayerPrefs.GetInt(key) < stars) {
				PlayerPrefs.SetInt(key, stars);
			}
		} else {
			PlayerPrefs.SetInt(key, stars);
		}

		Game.Data.Rushell.coins += Stars_Rank_Script.Data.roundCoins;
		PlayerPrefs.SetInt("coins", Game.Data.Rushell.coins);
		PlayerPrefs.Save();
		
		Application.LoadLevel("cena_levelSelect");
	}

	public void Pause() {
		if(Game.Data.Paused) {
			Camera.main.GetComponent<BlurOptimized>().enabled = false;
			Game.Data.Paused = false;
			Time.timeScale = 1;
		} else {
			Camera.main.GetComponent<BlurOptimized>().enabled = true;
			Game.Data.Paused = true;
			Time.timeScale = 0;
		}
	}
}
