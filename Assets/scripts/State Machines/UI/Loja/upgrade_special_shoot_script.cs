﻿using UnityEngine;
using UnityEngine.UI;

public class upgrade_special_shoot_script : MonoBehaviour {

	public string playerPrefsKey;

	public int buy_price;
	public int upgrade1_price;
	public int upgrade2_price;
	public int upgrade3_price;

	public int level;

	//UI
	public Text upgrade_button_text;
	public Button equip_button;
	public Text equip_button_text;

	public Image show_level1;
	public Image show_level2;
	public Image show_level3;
	public Image show_level4;

	// Use this for initialization
	void Start () {
//		if(PlayerPrefs.HasKey(playerPrefsKey)) {
//			level = PlayerPrefs.GetInt(playerPrefsKey);
//		} else {
//			level = 0;
//		}


	}
	
	// Update is called once per frame
	void Update () {
		level = 3;

		if(level == 0) {
			equip_button.enabled = false;
			equip_button_text.text = "Locked";
		} else {
			equip_button.enabled = true;
			equip_button_text.text = "Equip";
		}

		show_level1.enabled = level > 0;
		show_level2.enabled = level > 1;
		show_level3.enabled = level > 2;
		show_level4.enabled = level > 3;

		switch(level) {
		case 0:
			upgrade_button_text.text = "Buy "+buy_price.ToString();
			break;
		case 1:
			upgrade_button_text.text = "Upgrade "+upgrade1_price.ToString();
			break;
		case 2:
			upgrade_button_text.text = "Upgrade "+upgrade2_price.ToString();
			break;
		case 3:
			upgrade_button_text.text = "Upgrade "+upgrade3_price.ToString();
			break;
		case 4:
			upgrade_button_text.text = "Max Level";
			break;

		}
	}

	public void Upgrade() {
		if(level <= 3) {
			level++;
			PlayerPrefs.SetInt(playerPrefsKey,level);
			PlayerPrefs.Save();
		}
	}

	public void EraseProgress() {
		level = 0;
		PlayerPrefs.SetInt(playerPrefsKey,level);
		PlayerPrefs.Save();
	}
}
