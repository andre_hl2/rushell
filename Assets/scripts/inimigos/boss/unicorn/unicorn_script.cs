﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class unicorn_script : MonoBehaviour {

	public TrailRenderer trail;
	public int vida;
	public Slider vidaSlider;

	// Use this for initialization
	void Start () {
		vida = 100;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 pos = this.gameObject.transform.position;
		pos.z = -1;
		this.gameObject.transform.position = pos;

		vidaSlider.value = vida;

		if(vida <= 0) {
			Game.Data.deadBoss = true;
			Destroy (this.gameObject);
		}
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if(coll.gameObject.tag == "Player" && ! Game.Data.Rushell.invencible){
			if(!Game.Data.Rushell.dead) {
				Game.Data.Rushell.Animator.SetBool("Death",true);
			}
		} else if(coll.gameObject.tag == "tiro") {
			vida -= 10;
			Destroy(coll.gameObject);
		}

	}
}
