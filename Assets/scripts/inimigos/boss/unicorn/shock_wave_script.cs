﻿using UnityEngine;
using System.Collections;

public class shock_wave_script : MonoBehaviour {

	public float HSpeed;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 pos = this.gameObject.transform.position;
		pos.x += HSpeed * Time.deltaTime;
		this.gameObject.transform.position = pos;
		if(pos.x < -30 || pos.x > 30)
			Destroy(this.gameObject);
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if(coll.gameObject.tag == "Player" && !Game.Data.Rushell.invencible){
			if(!Game.Data.Rushell.dead) {
				Game.Data.Rushell.Animator.SetBool("Death",true);
			}
		}
	}
}
