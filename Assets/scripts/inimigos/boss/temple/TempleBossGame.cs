﻿using UnityEngine;
using System.Collections;

public class TempleBossGame : MonoBehaviour {

	public static TempleBossGame Data;
	public BossData Boss = new BossData();
	public bool canRock;

	public GameObject templeBackground;
	public GameObject normalBackground;

	public GameObject rain;

	public AudioClip thunder;

	public void Awake() {
		if (Data == null) {
			Data = this;
		}
	}

	// Use this for initialization
	void Start () {
		if(Game.Data.actualLevel == 15) {
			templeBackground.SetActive(true);
			rain.SetActive(false);
			normalBackground.SetActive(false);
			canRock = true;
		} else if(Game.Data.actualLevel == 10) {
			templeBackground.SetActive(false);
			rain.SetActive(true);
			normalBackground.SetActive(false);
			canRock = false;
		} else {
			templeBackground.SetActive(false);
			rain.SetActive(false);
			normalBackground.SetActive(true);
			canRock = false;
		}
	}

	public void playThunder() {
		AudioSource.PlayClipAtPoint(thunder, Camera.main.transform.position);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	[System.Serializable]
	public class BossData {
		public GameObject Root;
		private Rigidbody2D _rigidbody;
		private Animator _animator;

		public int vida;

		public Rigidbody2D RigidBody {
			get {
				if(_rigidbody == null && Root != null)
					_rigidbody = Root.GetComponent<Rigidbody2D>();
				return _rigidbody;
			}
		}

		public Animator Animator {
			get {
				if(_animator == null && Root != null)
					_animator = Root.GetComponent<Animator>();
				return _animator;
			}
		}
	}
}
