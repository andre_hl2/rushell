﻿using UnityEngine;
using System.Collections;

public class rock_fall_script : MonoBehaviour {

	public bool hit_ground;

	public float timeToFall;

	public GameObject fall_dust;

	// Use this for initialization
	void Start () {
		Instantiate(fall_dust, new Vector3(this.transform.position.x, 16, 0), Quaternion.identity);
	}
	
	// Update is called once per frame
	void Update () {
		timeToFall -= Time.deltaTime;

		if(timeToFall > 0) {
			this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
			Vector2 pos = this.transform.position;
			pos.y -= 0.02f;
			this.transform.position = pos;
		}
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if(!hit_ground) {
			if(coll.gameObject.tag == "tiro") {
				Destroy(coll.gameObject);
			} else if (coll.gameObject.tag == "Player") {
				if(!Game.Data.Rushell.dead) {
					Game.Data.Rushell.Animator.SetBool("Death",true);
				}
			} else if(coll.gameObject.tag == "ground") {
				hit_ground = true;
				this.GetComponent<Rigidbody2D>().velocity = new Vector2( Random.Range(-3.2f,3.2f), this.gameObject.GetComponent<Rigidbody2D>().velocity.y * -0.4f);
				if(this.GetComponent<Rigidbody2D>().velocity.x < 0) {
					this.gameObject.GetComponent<Rigidbody2D>().angularVelocity = 200;
				} else {
					this.gameObject.GetComponent<Rigidbody2D>().angularVelocity = -200;
				}
			}
		}
	}

}
