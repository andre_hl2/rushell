﻿using UnityEngine;
using System.Collections;

public class bounce_barrel_script : MonoBehaviour {

	public bool grounded;
	public LayerMask groundLayer;
	public float HSpeed;
	
	Rigidbody2D body;

	public Vector2 initialVelocity;

	// Use this for initialization
	void Start () {
		body = this.gameObject.GetComponent<Rigidbody2D>();
		body.velocity = initialVelocity;
	}
	
	// Update is called once per frame
	void Update () {
		grounded = isGrouded();

		body.angularVelocity = -500;
		
		if(this.transform.position.x > 15) {
			Destroy(this.gameObject);
		}
	}

	bool isGrouded() {
		RaycastHit2D hit = Physics2D.Raycast(this.transform.position, new Vector2(0,-1), 0.42f, groundLayer);
		if(hit.collider != null) {
			this.transform.position = new Vector2(this.transform.position.x, hit.point.y + 0.42f);
			body.velocity = new Vector2(body.velocity.x, - body.velocity.y);
			return true;
		} else {
			return false;
		}
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if(coll.gameObject.tag == "tiro") {
			Destroy(coll.gameObject);
			Destroy(this.gameObject);
		} else if (coll.gameObject.tag == "Player"  && !Game.Data.Rushell.invencible) {
			if(!Game.Data.Rushell.dead) {
				Game.Data.Rushell.Animator.SetBool("Death",true);
			}
		}
	}
}
