﻿using UnityEngine;
using System.Collections;

public class carrinho_script : MonoBehaviour {

	MoveWithCam move;
	public LayerMask afterLayer;

	public GameObject roda1;
	public GameObject roda2;
	public GameObject roda3;
	public GameObject roda4;
	public float rotateSpeed;

	public Sprite carAfterSprite;
	public Sprite afterSprite;

	// Use this for initialization
	void Start () {
		move = this.gameObject.GetComponent<MoveWithCam>();
		if(move != null)
			move.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(Game.Data.deadBoss) {
			if(move != null)
				move.enabled = true;
			this.gameObject.layer = afterLayer;

			this.gameObject.GetComponent<SpriteRenderer>().sprite = carAfterSprite;
			roda1.gameObject.GetComponent<SpriteRenderer>().sprite = afterSprite;
			roda2.gameObject.GetComponent<SpriteRenderer>().sprite = afterSprite;
			roda3.gameObject.GetComponent<SpriteRenderer>().sprite = afterSprite;
			roda4.gameObject.GetComponent<SpriteRenderer>().sprite = afterSprite;

			roda1.gameObject.GetComponent<Rigidbody2D>().angularVelocity = 0;
			roda2.gameObject.GetComponent<Rigidbody2D>().angularVelocity = 0;
			roda3.gameObject.GetComponent<Rigidbody2D>().angularVelocity = 0;
			roda4.gameObject.GetComponent<Rigidbody2D>().angularVelocity = 0;


		} else {
			if(Game.Data.cameraFollowing) {
				roda1.gameObject.GetComponent<Rigidbody2D>().angularVelocity = rotateSpeed;
				roda2.gameObject.GetComponent<Rigidbody2D>().angularVelocity = rotateSpeed;
				roda3.gameObject.GetComponent<Rigidbody2D>().angularVelocity = rotateSpeed;
				roda4.gameObject.GetComponent<Rigidbody2D>().angularVelocity = rotateSpeed;
			} else {
				roda1.gameObject.GetComponent<Rigidbody2D>().angularVelocity = 0;
				roda2.gameObject.GetComponent<Rigidbody2D>().angularVelocity = 0;
				roda3.gameObject.GetComponent<Rigidbody2D>().angularVelocity = 0;
				roda4.gameObject.GetComponent<Rigidbody2D>().angularVelocity = 0;
			}
		}
	}
}
