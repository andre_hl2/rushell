﻿using UnityEngine;
using System.Collections;

public class templeboss_state_machine_script : MonoBehaviour {

	public bool grounded;
	public LayerMask groundLayer;
	public bool dead;

	public GameObject bounce_barrel;
	public GameObject roll_barrel;
	
	// Use this for initialization
	void Start () {
		TempleBossGame.Data.Boss.vida = 100;
	}
	
	// Update is called once per frame
	void Update () {
		if(!dead) {
			grounded = isGrouded();
			if(!grounded) {
				this.gameObject.GetComponent<Rigidbody2D>().AddForce(Physics2D.gravity);
			}
			this.gameObject.GetComponent<Animator>().SetBool("Grounded",grounded);
		}

		if(TempleBossGame.Data.Boss.vida == 0){
			dead = true;
			this.gameObject.GetComponent<Animator>().SetBool("Death", true);
			TempleBossGame.Data.Boss.vida = -1;
		} else if(TempleBossGame.Data.Boss.vida > 0){
			dead = false;
		}


	}

	bool isGrouded() {
		RaycastHit2D hit = Physics2D.Raycast(this.transform.position, new Vector2(0,-1), 2.02f, groundLayer);
		if(hit.collider != null) {
			this.transform.position = new Vector2(this.transform.position.x, hit.point.y + 2.02f);
			this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
			return true;
		} else {
			return false;
		}
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if(!dead) {
			if(coll.gameObject.tag == "tiro") {
				if(Game.Data.actualLevel != 15) {
					TempleBossGame.Data.Boss.vida -= 10;
				} else {
					TempleBossGame.Data.Boss.vida -= 5;
				}

				if(TempleBossGame.Data.Boss.vida > 0)
					this.gameObject.GetComponent<Animator>().SetBool("TakeDamage",true);

				Destroy(coll.gameObject);
			}
			if (coll.gameObject.tag == "Player" && !Game.Data.Rushell.invencible) {
				if(!Game.Data.Rushell.dead) {
					Game.Data.Rushell.Animator.SetBool("Death",true);
				}
			}
		}

	}

	public void ThrowBounceBarrel() {
		Vector3 pos = this.gameObject.transform.position;
		Instantiate(bounce_barrel, new Vector3(pos.x + 2.39f, pos.y + 1.8f, pos.z), Quaternion.identity);
	}

	public void ThrowRollBarrel() {
		Vector3 pos = this.gameObject.transform.position;
		Instantiate(roll_barrel, new Vector3(pos.x + 2.67f, pos.y - 1.14f, pos.z), Quaternion.identity);
	}
}
