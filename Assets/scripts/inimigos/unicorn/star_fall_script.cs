﻿using UnityEngine;
using System.Collections;

public enum StarDirections { Left, Center, Right }

public class star_fall_script : MonoBehaviour {

	public StarDirections direction;
	public float visionDistance = 10;
	public GameObject rocks;

	Vector3 offset;

	// Use this for initialization
	void Start () {
		switch(direction) {
		case StarDirections.Left:
			break;
		case StarDirections.Center:
			offset = new Vector3(0,0.54f,0);
			this.transform.position -= new Vector3(0, 1.2f, 0);
			break;
		case StarDirections.Right:
			offset = new Vector3(0.25f, 0.58f,0);
			this.transform.position -= new Vector3(0.37f, 1.2f, 0);
			break;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(Game.Data.Rushell.Root.transform.position.x - this.transform.position.x < visionDistance) {
			this.transform.position = Vector3.Lerp(this.transform.position, rocks.transform.position + offset,0.3f);
		}
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if (coll.gameObject.tag == "Player"  && !Game.Data.Rushell.invencible) {
			if(!Game.Data.Rushell.dead) {
				Game.Data.Rushell.Animator.SetBool("Death",true);
			}
		} else if (coll.gameObject.tag == "tiro") {
			Destroy(coll.gameObject);
			Destroy(this.gameObject);
		}
	}
}
