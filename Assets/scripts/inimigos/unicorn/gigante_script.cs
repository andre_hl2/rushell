﻿using UnityEngine;
using System.Collections;

public class gigante_script : MonoBehaviour {

	public bool grounded;
	public LayerMask groundLayer;
	public bool dead;
	public bool attacking = false;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		grounded = isGrouded();

		if(!grounded) {
			this.gameObject.GetComponent<Rigidbody2D>().AddForce(Physics2D.gravity);
		}

		this.gameObject.GetComponent<BoxCollider2D>().enabled = !attacking;
		this.gameObject.GetComponent<CircleCollider2D>().enabled = attacking;
	}

	bool isGrouded() {
		if(attacking) {
			if(this.gameObject.GetComponent<Rigidbody2D>().velocity.y < 0) {
				RaycastHit2D hit = Physics2D.Raycast(this.transform.position, new Vector2(0,-1), 0.7f, groundLayer);
				if(hit.collider != null) {
					this.transform.position = new Vector2(this.transform.position.x, hit.point.y + 0.7f);
					this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
					attacking = false;
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			RaycastHit2D hit = Physics2D.Raycast(this.transform.position, new Vector2(0,-1), 0.08f, groundLayer);
			if(hit.collider != null) {
				this.transform.position = new Vector2(this.transform.position.x, hit.point.y + 0.08f);
				this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
				return true;
			} else {
				return false;
			}
		}

	}

	void OnTriggerEnter2D(Collider2D coll) {
		if(!dead) {
			if(coll.gameObject.tag == "tiro") {
				dead = true;
				Stars_Rank_Script.Data.roundEnemies++;
				this.gameObject.GetComponent<Animator>().SetBool("Death",true);
				Destroy(coll.gameObject);
				Destroy(this.gameObject);
			} else if (coll.gameObject.tag == "Player"  && !Game.Data.Rushell.invencible) {
				if(!Game.Data.Rushell.dead) {
					if(Game.Data.Rushell.pounding) {
						this.gameObject.GetComponent<Animator>().SetBool("Death",true);
						dead = true;
						Stars_Rank_Script.Data.roundEnemies++;
						Destroy(this.gameObject);
					} else {
						Game.Data.Rushell.Animator.SetBool("Death",true);
					}
				}
			}
		}
	}
}
