﻿using UnityEngine;
using System.Collections;

public class fada_fogo_script : MonoBehaviour {

	public GameObject projetil;
	public bool dead;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(dead) {
			SpriteRenderer ren = this.gameObject.GetComponent<SpriteRenderer>();
			if(ren != null) {
				Color col = ren.material.color;
				col.a -= 0.01f;
				this.gameObject.GetComponent<SpriteRenderer>().material.color = col;
			}
		}
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if(!dead) {
			if(coll.gameObject.tag == "tiro") {
				dead = true;
				Stars_Rank_Script.Data.roundEnemies++;
				this.gameObject.GetComponent<Animator>().SetBool("Death",true);
				Destroy(coll.gameObject);
			} else if (coll.gameObject.tag == "Player"  && !Game.Data.Rushell.invencible) {
				if(!Game.Data.Rushell.dead) {
					if(Game.Data.Rushell.pounding) {
						this.gameObject.GetComponent<Animator>().SetBool("Death",true);
						dead = true;
						Stars_Rank_Script.Data.roundEnemies++;
					} else {
						Game.Data.Rushell.Animator.SetBool("Death",true);
					}
				}
			}
		}
	}

	public void Attack() {
		if(!Game.Data.Rushell.dead && (Game.Data.Rushell.Root.transform.position.x > this.gameObject.transform.position.x + 2)) {
			GameObject clone = (GameObject) Instantiate(projetil,this.gameObject.transform.position, Quaternion.identity);
		}
	}
}
