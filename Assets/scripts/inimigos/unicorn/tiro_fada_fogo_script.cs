﻿using UnityEngine;
using System.Collections;

public class tiro_fada_fogo_script : MonoBehaviour {

	public Vector2 movement;
	public float speed;

	// Use this for initialization
	void Start () {
		movement = Game.Data.Rushell.Root.transform.position - this.gameObject.transform.position;
		movement.Normalize();
	}
	
	// Update is called once per frame
	void Update () {
		Vector2 pos = this.gameObject.transform.position;
		pos += movement * speed * Time.deltaTime;
		this.gameObject.transform.position = pos;
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if(coll.gameObject.tag == "Player" && !Game.Data.Rushell.invencible) {
			if(!Game.Data.Rushell.dead) {
				if(Game.Data.Rushell.pounding) {
					Destroy(this.gameObject);
				} else {
					Game.Data.Rushell.Animator.SetBool("Death",true);
					Destroy(this.gameObject);
				}
			}
		}
	}
}
