﻿using UnityEngine;
using System.Collections;

public class nave_gigante_script : MonoBehaviour {

	public GameObject mina;
	public float initialTime;
	public float timeBetweenMinas;
	float actualTime;
	public float HSpeed;
	public float LeftLimit;

	// Use this for initialization
	void Start () {
		actualTime = initialTime;
	}
	
	// Update is called once per frame
	void Update () {
		actualTime -= Time.deltaTime;
		if(actualTime < 0) {
			GameObject clone = (GameObject)Instantiate(mina, this.gameObject.transform.position, Quaternion.identity);
			clone.AddComponent<MoveWithCam>();
			actualTime = timeBetweenMinas;
		}
		Vector3 pos = this.gameObject.transform.position;
		pos.x -= HSpeed * Time.deltaTime;
		this.gameObject.transform.position = pos;
		if(this.gameObject.transform.position.x < LeftLimit)
			Destroy(this.gameObject);
	}
}
