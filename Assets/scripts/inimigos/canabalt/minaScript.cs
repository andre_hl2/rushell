﻿using UnityEngine;
using System.Collections;

public class minaScript : MonoBehaviour {

	public bool grounded;
	public LayerMask groundLayer;

	public float visionDistance;
	public int time;
	public TextMesh txt;

	public float distanceExplosion;

	public float timeToLess;

	// Use this for initialization
	void Start () {
		time = 3;
		timeToLess = 0.4f;
	}
	
	// Update is called once per frame
	void Update () {
		grounded = isGrouded();

		if(!grounded) {
			this.gameObject.GetComponent<Rigidbody2D>().AddForce(Physics2D.gravity);
		}

		if((Game.Data.Rushell.Root.transform.position.x - this.gameObject.transform.position.x) < visionDistance) {
			timeToLess-=Time.deltaTime;
			if(timeToLess<0){
				time -= 1;
				if(time == 0){
					Explode();
				} else {
					timeToLess = 0.4f;
				}
			}
		}
		txt.text = time.ToString();
	}

	bool isGrouded() {
		RaycastHit2D hit = Physics2D.Raycast(this.transform.position, new Vector2(0,-1), 0.64f, groundLayer);
		if(hit.collider != null) {
			this.transform.position = new Vector2(this.transform.position.x, hit.point.y + 0.64f);
			this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
			return true;
		} else {
			return false;
		}
	}

	void Explode() {
		Collider2D[] res = Physics2D.OverlapCircleAll(this.transform.position, distanceExplosion);
		foreach(Collider2D coll in res) {
			if(coll.gameObject.tag == "Player" && !Game.Data.Rushell.invencible) {
				if(!Game.Data.Rushell.dead) {
					Game.Data.Rushell.Animator.SetBool("Death",true);
				}
			}
		}
		Destroy(this.gameObject);
	}
}
