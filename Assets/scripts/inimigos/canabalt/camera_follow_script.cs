﻿using UnityEngine;
using System.Collections;

public class camera_follow_script : MonoBehaviour {

	public LineRenderer line;

	public float distanceLook;
	public float lookTime;
	public float chargeTime;
	public float attackTime;
	public int state = 0;

	public LayerMask layer;

	public Vector2 dir;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);

		float angle;

		switch(state) {
		case 0: //olhando
			dir = (Game.Data.Rushell.Root.transform.position - this.gameObject.transform.position);
			angle = (Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg) + 180;

			if(angle < 120)
				angle = 120;
			if(angle > 220)
				angle = 220;
			this.transform.rotation = Quaternion.AngleAxis(angle, new Vector3(0,0,1));


			if((Game.Data.Rushell.Root.transform.position.x - this.gameObject.transform.position.x) < distanceLook) {
				lookTime -= Time.deltaTime;
				if(lookTime < 0) {
					state = 1;
				}
			}

			break;
		case 1:

			Vector3 sca = this.gameObject.transform.localScale;
			sca.y = 2;
			this.gameObject.transform.localScale = sca;

			chargeTime -= Time.deltaTime;
			if(chargeTime < 0) {
				state = 2;
				this.gameObject.transform.localScale = new Vector3(1,1.35f,1.35f);
			}

			break;
		case 2:
			attackTime -= Time.deltaTime;

			if(attackTime < 0) {
				state = 3;
				line.enabled = false;
			}

			Debug.DrawRay(this.transform.position, dir, Color.red);

			RaycastHit2D hit = Physics2D.Raycast(this.gameObject.transform.position, dir,100, layer);
			if(hit.collider != null) {

				if(hit.collider.gameObject.tag == "Player" && !Game.Data.Rushell.invencible) {
					if(!Game.Data.Rushell.dead) {
						Game.Data.Rushell.Animator.SetBool("Death",true);
					}
				}

				Vector3 ini = this.gameObject.transform.position;

				ini += (new Vector3(dir.x, dir.y, 0) *0.1f);

				line.SetPosition(0,new Vector3(ini.x,ini.y,-3));
				line.SetPosition(1,new Vector3(hit.point.x, hit.point.y, -3));
			}

			break;
		case 3:
			break;
		}
	}
}
