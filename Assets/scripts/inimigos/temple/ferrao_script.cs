﻿using UnityEngine;
using System.Collections;

public class ferrao_script : MonoBehaviour {

	public float speed;

	// Use this for initialization
	void Start () {
		Vector3 target = new Vector3(Game.Data.Rushell.Root.transform.position.x - 5, Game.Data.Rushell.Root.transform.position.y, Game.Data.Rushell.Root.transform.position.z);
		Vector2 dir = target - this.transform.position;
		dir.Normalize();
		dir *= speed;
		this.gameObject.GetComponent<Rigidbody2D>().velocity = dir;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if (coll.gameObject.tag == "Player"  && !Game.Data.Rushell.invencible) {
			if(!Game.Data.Rushell.dead) {
				Game.Data.Rushell.Animator.SetBool("Death",true);
			}
		} else if (coll.gameObject.tag == "ground") {
			Destroy(this.gameObject);
		}
	}
}
