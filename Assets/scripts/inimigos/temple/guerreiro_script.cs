﻿using UnityEngine;
using System.Collections;

public class guerreiro_script : MonoBehaviour {

	public bool grounded;
	public LayerMask groundLayer;
	public bool dead;

	public float searchArea;

	// Use this for initialization
	void Start () {
		dead = false;
	}
	
	// Update is called once per frame
	void Update () {
		grounded = isGrouded();

		if(!grounded) {
			this.gameObject.GetComponent<Rigidbody2D>().AddForce(Physics2D.gravity);
		}

		if((Game.Data.Rushell.Root.transform.position.x - this.transform.position.x) < searchArea) {
			if(!Game.Data.Rushell.dead) {
				if(Game.Data.Rushell.Root.transform.position.y - this.transform.position.y > 1) {
					this.gameObject.GetComponent<Animator>().SetBool("UpperAttack",true);
					this.gameObject.GetComponent<Animator>().SetBool("Attack",false);
				} else {
					this.gameObject.GetComponent<Animator>().SetBool("UpperAttack", false);
					this.gameObject.GetComponent<Animator>().SetBool("Attack",true);
				}
			} else {
				this.gameObject.GetComponent<Animator>().SetBool("Attack",false);
				this.gameObject.GetComponent<Animator>().SetBool("UpperAttack", false);
			} 
		} else {
			this.gameObject.GetComponent<Animator>().SetBool("Attack",false);
		}

	}

	bool isGrouded() {
		RaycastHit2D hit = Physics2D.Raycast(this.transform.position, new Vector2(0,-1), 2.20f, groundLayer);
		if(hit.collider != null) {
			this.transform.position = new Vector2(this.transform.position.x, hit.point.y + 2.20f);
			this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
			return true;
		} else {
			return false;
		}
	}

	
	void OnTriggerEnter2D(Collider2D coll) {
		if(!dead) {
			if(coll.gameObject.tag == "tiro") {
				dead = true;
				Stars_Rank_Script.Data.roundEnemies++;
				Destroy(coll.gameObject);
				
				//ALTERAR ISSO
				Destroy(this.gameObject);
			} else if (coll.gameObject.tag == "Player") {
				if(!Game.Data.Rushell.dead) {
					if(Game.Data.Rushell.pounding) {
						Stars_Rank_Script.Data.roundEnemies++;
						Destroy(this.gameObject);
					} else {
						Game.Data.Rushell.Animator.SetBool("Death",true);
					}
				}
			}
		}
	}

	public void Kill() {
		dead = true;
	}

	public void Attack() {
		Collider2D[] colls = Physics2D.OverlapCircleAll(new Vector2(this.transform.position.x + 1.24f, this.transform.position.y),1.0f);
		foreach (Collider2D coll in colls) {
			if(coll.gameObject.tag == "Player"  && !Game.Data.Rushell.invencible) {
				if(!Game.Data.Rushell.dead) {
					Game.Data.Rushell.Animator.SetBool("Death",true);
				}
			}
		}
	}

	public void upperAttack() {
		Collider2D[] colls = Physics2D.OverlapCircleAll(new Vector2(this.transform.position.x - 0.2f, this.transform.position.y + 1.4f),1.22f);
		foreach (Collider2D coll in colls) {
			if(coll.gameObject.tag == "Player"  && !Game.Data.Rushell.invencible) {
				if(!Game.Data.Rushell.dead) {
					Game.Data.Rushell.Animator.SetBool("Death",true);
				}
			}
		}
	}
}
