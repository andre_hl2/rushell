﻿using UnityEngine;
using System.Collections;

public class armadilha_script : MonoBehaviour {

	public Sprite closed_trap;

	public bool grounded;
	public LayerMask groundLayer;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		grounded = isGrouded();
		if(!grounded) {
			this.gameObject.GetComponent<Rigidbody2D>().AddForce(Physics2D.gravity);
		}
	}

	bool isGrouded() {
		RaycastHit2D hit = Physics2D.Raycast(this.transform.position, new Vector2(0,-1), 0.67f, groundLayer);
		if(hit.collider != null) {
			this.transform.position = new Vector2(this.transform.position.x, hit.point.y + 0.67f);
			this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
			return true;
		} else {
			return false;
		}
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if (coll.gameObject.tag == "Player"  && !Game.Data.Rushell.invencible) {
			if(!Game.Data.Rushell.dead) {
				Game.Data.Rushell.Animator.SetBool("Death",true);
				this.gameObject.GetComponent<SpriteRenderer>().sprite = closed_trap;
			}
		}
	}
}
