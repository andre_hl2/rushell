﻿using UnityEngine;
using System.Collections;

public class morte_queda_script : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if(coll.gameObject.tag == "tiro") {
			Destroy(coll.gameObject);
		} else if (coll.gameObject.tag == "Player") {
			if(!Game.Data.Rushell.dead) {
				Game.Data.Rushell.Animator.SetBool("Death",true);
			}
		}
	}
}
