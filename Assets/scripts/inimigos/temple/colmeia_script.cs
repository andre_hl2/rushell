﻿using UnityEngine;
using System.Collections;

public class colmeia_script : MonoBehaviour {

	public GameObject plataform;

	public bool grounded;
	public LayerMask groundLayer;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(plataform == null) {
			grounded = isGrouded();
			if(!grounded) {
				this.gameObject.GetComponent<Rigidbody2D>().AddForce(Physics2D.gravity);
			}
		} else {
			this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
		}
	}

	bool isGrouded() {
		RaycastHit2D hit = Physics2D.Raycast(this.transform.position, new Vector2(0,-1), 0.46f, groundLayer);
		if(hit.collider != null) {
			this.transform.position = new Vector2(this.transform.position.x, hit.point.y + 0.46f);
			this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
			return true;
		} else {
			return false;
		}
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if(coll.gameObject.tag == "tiro") {
			Destroy(coll.gameObject);
			Destroy(this.gameObject);
		} else if (coll.gameObject.tag == "Player"  && !Game.Data.Rushell.invencible) {
			if(!Game.Data.Rushell.dead && !Game.Data.Rushell.invencible) {
				Game.Data.Rushell.Animator.SetBool("Death",true);
			}
		}
	}
}
