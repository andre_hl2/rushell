﻿using UnityEngine;
using System.Collections;

public class pedra_gigante_script : MonoBehaviour {

	public float DistanceOffset;
	public float HSpeed;
	public LayerMask groundLayer;
	public bool grounded;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		grounded = isGrouded();
		
		if(!grounded) {
			this.gameObject.GetComponent<Rigidbody2D>().AddForce(Physics2D.gravity);
		}

		if((Game.Data.Rushell.Root.transform.position.x - this.transform.position.x) < DistanceOffset) {
			//ATACA
			this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(HSpeed, this.gameObject.GetComponent<Rigidbody2D>().velocity.y);
			this.gameObject.GetComponent<Rigidbody2D>().angularVelocity = -360;
		} else {
			this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
		}
	}

	bool isGrouded() {
		RaycastHit2D hit = Physics2D.Raycast(this.transform.position, new Vector2(0,-1), 1.45f, groundLayer);
		if(hit.collider != null) {
			this.transform.position = new Vector2(this.transform.position.x, hit.point.y + 1.45f);
			this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
			return true;
		} else {
			return false;
		}
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if(coll.gameObject.tag == "Player"  && !Game.Data.Rushell.invencible) {
			if(!Game.Data.Rushell.dead) {
				Game.Data.cameraFollowing = false;
				Game.Data.Rushell.Animator.SetInteger("Death_Type",2);
				Game.Data.Rushell.Animator.SetBool("Death",true);
			}
		}
	}
}
