﻿using UnityEngine;
using System.Collections;

public class aranha_script : MonoBehaviour {

	public bool grounded;
	public LayerMask groundLayer;
	public bool dead;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		grounded = isGrouded();

		if(!grounded) {
			this.gameObject.GetComponent<Rigidbody2D>().AddForce(Physics2D.gravity);
		}
	}

	bool isGrouded() {
		RaycastHit2D hit = Physics2D.Raycast(this.transform.position, new Vector2(0,-1), 1.25f, groundLayer);
		if(hit.collider != null) {
			this.transform.position = new Vector2(this.transform.position.x, hit.point.y + 1.25f);
			this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
			return true;
		} else {
			return false;
		}
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if(!dead) {
			if(coll.gameObject.tag == "tiro") {
				dead = true;
				Stars_Rank_Script.Data.roundEnemies++;
				this.gameObject.GetComponent<Animator>().SetBool("Death",true);
				Destroy(coll.gameObject);
			} else if (coll.gameObject.tag == "Player"  && !Game.Data.Rushell.invencible) {
				if(!Game.Data.Rushell.dead) {
					if(Game.Data.Rushell.pounding) {
						this.gameObject.GetComponent<Animator>().SetBool("Death",true);
						dead = true;
						Stars_Rank_Script.Data.roundEnemies++;
					} else {
						Game.Data.Rushell.Animator.SetBool("Death",true);
					}
				}
			}
		}
	}

	public void Kill() {
		dead = true;
	}
}
