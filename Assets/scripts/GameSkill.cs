﻿
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections;

public class GameSkill : MonoBehaviour {

	public static GameSkill Data;
	
//	public GameObject SkillsModal;
//	public bool showSkillsModal;
//
//	public GameObject SkillModal;
//	public Text name_SkillModal;
//	public bool choosingSkill;
//	public int skillChossing;
//
//
//	//SKILLS MODAL OPTIONS
//	public Image doubleJumpImage;
//	public int doubleJumpOption;
//	public Image SwipeImage;
//	public int SwipeOption;
//	public Image Jump_ShootImage;
//	public int Jump_ShootOption;
//	public Image Jump_SwipeImage;
//	public int Jump_SwipeOption;
//
//	//SKILL MODAL OPTIONS
//	public int option;
//
//	public Image ActualSkill;
//	public Image spriteOption1;
//	public Image spriteOption2;
//	public Image spriteOption3;
//
//	public Text txt_quantityActual;
//	public Text txt_quantityOption1;
//	public Text txt_quantityOption2;
//	public Text txt_quantityOption3;
//
//	int quantityOption1;
//	int quantityOption2;
//	int quantityOption3;
//
//	public SkillOption actualSkillOption;
//	public SkillOption doubleTap;
//	public SkillOption Swipe;
//	public SkillOption Jump_Shoot;
//	public SkillOption Jump_Swipe;



	public bool showingShop = false;
	public int tabSelected = 1;
	public int subTabSeleted = 1;

	public Image icon1;
	public Image icon2;
	public Image icon3;

	public Text txt1;
	public Text txt2;
	public Text txt3;

	public Text btnTxt1;
	public Text btnTxt2;
	public Text btnTxt3;

	int swipe_powerUp;
	int doubleJump_powerUp;
	int jumoShoot_powerUp;
	int jumpSwipe_powerUp;

	public SkillOption[] options_holder;

	public Text WeaponName;
	public Text WeaponDescription;
	public Text weaponBtnText;

	public bool has_special_weapon;
	public int weapon;

	public void Awake() {
		if(Data == null) {
			Data = this;
		}
	}

	// Use this for initialization
	void Start () {
		if(PlayerPrefs.HasKey("hasSpecialWeapon")) {
			has_special_weapon = (PlayerPrefs.GetInt("hasSpecialWeapon") == 1);
		} else {
			has_special_weapon = false;
		}
		
		if(has_special_weapon) {
			if(PlayerPrefs.HasKey("specialWeapon")) {
				weapon = PlayerPrefs.GetInt("specialWeapon");
			} else {
				has_special_weapon = false;
				weapon = 0;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {

		//update dos botoes de power ups
		if(GameSkill.Data.showingShop) {
			if(GameSkill.Data.tabSelected == 1) {
				int option = 0;
				switch(GameSkill.Data.subTabSeleted){
				case 1:
					option = swipe_powerUp;
					break;
				case 2:
					option = doubleJump_powerUp;
					break;
				case 3:
					option = jumoShoot_powerUp;
					break;
				case 4:
					option = jumpSwipe_powerUp;
					break;
				}

				if(option != 0){
					btnTxt1.text = "Equip";
					btnTxt2.text = "Equip";
					btnTxt3.text = "Equip";

					switch(option){
					case 1:
						btnTxt1.text = "Equipped";
						break;
					case 2:
						btnTxt2.text = "Equipped";
						break;
					case 3:
						btnTxt3.text = "Equipped";
						break;
					}
				}
			}
		}

	}


	public void SwitchShop() {
		showingShop = !showingShop;
	}

	public void changeTab(int tab) {
		tabSelected = tab;
		changeSubTab(1);
	}

	public void changeSubTab(int tab) {
		subTabSeleted = tab;

		if(tabSelected == 1) {
			SkillOption option = options_holder[tab -1];

			icon1.sprite = option.option1;
			icon2.sprite = option.option2;
			icon3.sprite = option.option3;

			txt1.text    = option.txt1;
			txt2.text    = option.txt2;
			txt3.text    = option.txt3;

			if(PlayerPrefs.HasKey("pwu_double_tap"))
				doubleJump_powerUp = PlayerPrefs.GetInt("pwu_double_tap");
			else
				doubleJump_powerUp = 0;
			if(PlayerPrefs.HasKey("pwu_swipe"))
				swipe_powerUp = PlayerPrefs.GetInt("pwu_swipe");
			else
				swipe_powerUp = 0;
			if(PlayerPrefs.HasKey("pwu_tap_shoot"))
				jumoShoot_powerUp = PlayerPrefs.GetInt("pwu_tap_shoot");
			else
				jumoShoot_powerUp = 0;

			if(PlayerPrefs.HasKey("pwu_tap_swipe"))
				jumpSwipe_powerUp = PlayerPrefs.GetInt("pwu_tap_swipe");
			else
				jumpSwipe_powerUp = 0;
		} else if(tabSelected == 2) {

			if(weapon == tab) {
				weaponBtnText.text = "Equiped";
			} else {
				weaponBtnText.text = "Equip";
			}

			switch(tab) {
			case 1:
				WeaponName.text = "Triple Shoot";
				WeaponDescription.text = "When Rushell fires his bazooka, three bombs come out of it";
				break;
			case 2:
				WeaponName.text = "Black Hole Shoot";
				WeaponDescription.text = "When it hits the ground, it explodes in a black hole that pulls everything to its center";
				break;
			case 3:
				WeaponName.text = "Storm Shoot";
				WeaponDescription.text = "When it hits the ground, creates a violent storm that makes everything fly away";
				break;
			case 4:
				WeaponName.text = "Sequential Shoot";
				WeaponDescription.text = "Rushell shoots three sequential bombs in one direction";
				break;
			case 5:
				WeaponName.text = "Golden Shoot";
				WeaponDescription.text = "If it hits the ground, it spawns a little amount of coins, if it kill an enemy, spawns a large amount of gold";
				break;
			case 6:
				WeaponName.text = "Firework Shoot";
				WeaponDescription.text = "When it hits the ground, it explodes in a Firework effect that destroy everything around";
				break;
			case 7:
				WeaponName.text = "Homing Missile";
				WeaponDescription.text = "This Shoot follows the nearby enemy";
				break;
			}
		}
	}

	public void EquipPowerUp(int powerUp) {
		string key = "";
		int option = powerUp;
		switch(GameSkill.Data.subTabSeleted){
		case 1:
			key = "pwu_swipe";
			swipe_powerUp = powerUp;
			break;
		case 2:
			key = "pwu_double_tap";
			doubleJump_powerUp = powerUp;
			break;
		case 3:
			key = "pwu_tap_shoot";
			jumoShoot_powerUp = powerUp;
			break;
		case 4:
			key = "pwu_tap_swipe";
			jumpSwipe_powerUp = powerUp;
			break;
		}
		PlayerPrefs.SetInt(key, option);
		PlayerPrefs.Save();
	}

	public void EquipWeapon() {
		has_special_weapon = true;
		weapon = GameSkill.Data.subTabSeleted;

		weaponBtnText.text = "Equiped";

		PlayerPrefs.SetInt("hasSpecialWeapon",1);
		PlayerPrefs.SetInt("specialWeapon",weapon);
		PlayerPrefs.Save();
	}


	[System.Serializable]
	public class SkillOption {
		public string txt1;
		public Sprite option1;
		public string txt2;
		public Sprite option2;
		public string txt3;
		public Sprite option3;
	}
















//	void UpdateSkillImage(bool load) {
//		if(load) {
//			if(PlayerPrefs.HasKey("pwu_double_tap"))
//				doubleJumpOption = PlayerPrefs.GetInt("pwu_double_tap");
//			else
//				doubleJumpOption = 0;
//
//			if(PlayerPrefs.HasKey("pwu_swipe"))
//				SwipeOption = PlayerPrefs.GetInt("pwu_swipe");
//			else
//				SwipeOption = 0;
//
//			if(PlayerPrefs.HasKey("pwu_tap_shoot"))
//				Jump_ShootOption = PlayerPrefs.GetInt("pwu_tap_shoot");
//			else
//				Jump_ShootOption = 0;
//
//			if(PlayerPrefs.HasKey("pwu_tap_swipe"))
//				Jump_SwipeOption = PlayerPrefs.GetInt("pwu_tap_swipe");
//			else
//				Jump_SwipeOption = 0;
//		}
//
//		switch(doubleJumpOption) {
//		case 1:
//			doubleJumpImage.sprite = doubleTap.option1;
//			break;
//		case 2:
//			doubleJumpImage.sprite = doubleTap.option2;
//			break;
//		case 3:
//			doubleJumpImage.sprite = doubleTap.option3;
//			break;
//		}
//
//		switch(SwipeOption) {
//		case 1:
//			SwipeImage.sprite = Swipe.option1;
//			break;
//		case 2:
//			SwipeImage.sprite = Swipe.option2;
//			break;
//		case 3:
//			SwipeImage.sprite = Swipe.option3;
//			break;
//		}
//
//		switch(Jump_ShootOption) {
//		case 1:
//			Jump_ShootImage.sprite = Jump_Shoot.option1;
//			break;
//		case 2:
//			Jump_ShootImage.sprite = Jump_Shoot.option2;
//			break;
//		case 3:
//			Jump_ShootImage.sprite = Jump_Shoot.option3;
//			break;
//		}
//
//		switch(Jump_SwipeOption) {
//		case 1:
//			Jump_SwipeImage.sprite = Jump_Swipe.option1;
//			break;
//		case 2:
//			Jump_SwipeImage.sprite = Jump_Swipe.option2;
//			break;
//		case 3:
//			Jump_SwipeImage.sprite = Jump_Swipe.option3;
//			break;
//		}
//	}

//	public void HideSkillsModal() {
//
//		PlayerPrefs.SetInt("pwu_double_tap",doubleJumpOption);
//		PlayerPrefs.SetInt("pwu_swipe",SwipeOption);
//		PlayerPrefs.SetInt("pwu_tap_shoot",Jump_ShootOption);
//		PlayerPrefs.SetInt("pwu_tap_swipe",Jump_SwipeOption);
//		PlayerPrefs.Save();
//
//		showSkillsModal = false;
////		SkillsModal.gameObject.transform.DOScale(0,0.5f).SetEase(Ease.InOutElastic).OnComplete( () => {
////			showSkillsModal = false; SkillModal.transform.localScale = new Vector3(0,0,0);
////		});
//	}

//	public void ShowSkillModal(int skill) {
//		choosingSkill = true;
//		skillChossing = skill;
//		switch(skill) {
//		case 1:
//			name_SkillModal.text = "Double Tap";
//			actualSkillOption = doubleTap;
//
//			if(PlayerPrefs.HasKey("pwu_double_tap"))
//				option = PlayerPrefs.GetInt("pwu_double_tap");
//			else
//				option = 0;
//
//			if(PlayerPrefs.HasKey("pwu_double_tap_qtd_1"))
//				quantityOption1 = PlayerPrefs.GetInt("pwu_double_tap_qtd_1");
//			else
//				quantityOption1 = 10;
//
//			if(PlayerPrefs.HasKey("pwu_double_tap_qtd_2"))
//				quantityOption2 = PlayerPrefs.GetInt("pwu_double_tap_qtd_2");
//			else
//				quantityOption2 = 10;
//
//			if(PlayerPrefs.HasKey("pwu_double_tap_qtd_3"))
//				quantityOption3 = PlayerPrefs.GetInt("pwu_double_tap_qtd_3");
//			else
//				quantityOption3 = 10;
//
//			spriteOption1.sprite = doubleTap.option1;
//			spriteOption2.sprite = doubleTap.option2;
//			spriteOption3.sprite = doubleTap.option3;
//			break;
//		case 2:
//
//			name_SkillModal.text = "Swipe";
//			actualSkillOption = Swipe;
//
//			if(PlayerPrefs.HasKey("pwu_swipe"))
//				option = PlayerPrefs.GetInt("pwu_swipe");
//			else
//				option = 0;
//
//			if(PlayerPrefs.HasKey("pwu_swipe_qtd_1"))
//				quantityOption1 = PlayerPrefs.GetInt("pwu_swipe_qtd_1");
//			else
//				quantityOption1 = 10;
//
//			if(PlayerPrefs.HasKey("pwu_swipe_qtd_2"))
//				quantityOption2 = PlayerPrefs.GetInt("pwu_swipe_qtd_2");
//			else
//				quantityOption2 = 10;
//
//			if(PlayerPrefs.HasKey("pwu_swipe_qtd_3"))
//				quantityOption3 = PlayerPrefs.GetInt("pwu_swipe_qtd_3");
//			else
//				quantityOption3 = 10;
//
//			spriteOption1.sprite = Swipe.option1;
//			spriteOption2.sprite = Swipe.option2;
//			spriteOption3.sprite = Swipe.option3;
//			break;
//		case 3:
//			name_SkillModal.text = "Jump and Shoot";
//			actualSkillOption = Jump_Shoot;
//
//			if(PlayerPrefs.HasKey("pwu_tap_shoot"))
//				option = PlayerPrefs.GetInt("pwu_tap_shoot");
//			else
//				option = 0;
//
//			if(PlayerPrefs.HasKey("pwu_tap_shoot_qtd_1"))
//				quantityOption1 = PlayerPrefs.GetInt("pwu_tap_shoot_qtd_1");
//			else
//				quantityOption1 = 10;
//
//			if(PlayerPrefs.HasKey("pwu_tap_shoot_qtd_2"))
//				quantityOption2 = PlayerPrefs.GetInt("pwu_tap_shoot_qtd_2");
//			else
//				quantityOption2 = 10;
//
//			if(PlayerPrefs.HasKey("pwu_tap_shoot_qtd_3"))
//				quantityOption3 = PlayerPrefs.GetInt("pwu_tap_shoot_qtd_3");
//			else
//				quantityOption3 = 10;
//
//			spriteOption1.sprite = Jump_Shoot.option1;
//			spriteOption2.sprite = Jump_Shoot.option2;
//			spriteOption3.sprite = Jump_Shoot.option3;
//			break;
//		case 4:
//			name_SkillModal.text = "Jump and Swipe";
//			actualSkillOption = Jump_Swipe;
//
//			if(PlayerPrefs.HasKey("pwu_tap_swipe"))
//				option = PlayerPrefs.GetInt("pwu_tap_swipe");
//			else
//				option = 0;
//
//			if(PlayerPrefs.HasKey("pwu_tap_swipe_qtd_1"))
//				quantityOption1 = PlayerPrefs.GetInt("pwu_tap_swipe_qtd_1");
//			else
//				quantityOption1 = 10;
//
//			if(PlayerPrefs.HasKey("pwu_tap_swipe_qtd_2"))
//				quantityOption2 = PlayerPrefs.GetInt("pwu_tap_swipe_qtd_2");
//			else
//				quantityOption2 = 10;
//
//			if(PlayerPrefs.HasKey("pwu_tap_swipe_qtd_3"))
//				quantityOption3 = PlayerPrefs.GetInt("pwu_tap_swipe_qtd_3");
//			else
//				quantityOption3 = 10;
//
//			spriteOption1.sprite = Jump_Swipe.option1;
//			spriteOption2.sprite = Jump_Swipe.option2;
//			spriteOption3.sprite = Jump_Swipe.option3;
//			break;
//		}
//		SkillModal.gameObject.transform.localScale = new Vector3(1,1,1);
//		//SkillModal.gameObject.transform.DOScale(1,0.5f).SetEase(Ease.OutBounce,0.2f);
//	}

//	public void HideSSkillModal() {
//		UpdateSkillImage(true);
//		switch(skillChossing) {
//		case 1:
//			doubleJumpOption = option;
//			PlayerPrefs.SetInt("psw_double_tap",option);
//			PlayerPrefs.SetInt("pwu_double_tap_qtd_1",quantityOption1);
//			PlayerPrefs.SetInt("pwu_double_tap_qtd_2",quantityOption1);
//			PlayerPrefs.SetInt("pwu_double_tap_qtd_3",quantityOption1);
//			PlayerPrefs.Save();
//			break;
//		case 2:
//			SwipeOption = option;
//			PlayerPrefs.SetInt("pwu_swipe_qtd_1",quantityOption1);
//			PlayerPrefs.SetInt("pwu_swipe_qtd_2",quantityOption1);
//			PlayerPrefs.SetInt("pwu_swipe_qtd_3",quantityOption1);
//			PlayerPrefs.Save();
//			break;
//		case 3:
//			Jump_ShootOption = option;
//			PlayerPrefs.SetInt("pwu_tap_shoot_qtd_1",quantityOption1);
//			PlayerPrefs.SetInt("pwu_tap_shoot_qtd_2",quantityOption1);
//			PlayerPrefs.SetInt("pwu_tap_shoot_qtd_3",quantityOption1);
//			PlayerPrefs.Save();
//			break;
//		case 4:
//			Jump_SwipeOption = option;
//			PlayerPrefs.SetInt("pwu_tap_swipe_qtd_1",quantityOption1);
//			PlayerPrefs.SetInt("pwu_tap_swipe_qtd_2",quantityOption1);
//			PlayerPrefs.SetInt("pwu_tap_swipe_qtd_3",quantityOption1);
//			PlayerPrefs.Save();
//			break;
//		}
//		PlayerPrefs.SetInt("pwu_double_tap",doubleJumpOption);
//		PlayerPrefs.SetInt("pwu_swipe",SwipeOption);
//		PlayerPrefs.SetInt("pwu_tap_shoot",Jump_ShootOption);
//		PlayerPrefs.SetInt("pwu_tap_swipe",Jump_SwipeOption);
//
//		choosingSkill = false;
//		//SkillModal.gameObject.transform.DOScale(0,0.5f).SetEase(Ease.InOutElastic).OnComplete( () => choosingSkill = false );
//
//	}
}
