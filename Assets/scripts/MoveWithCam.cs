﻿using UnityEngine;
using System.Collections;

public class MoveWithCam : MonoBehaviour {

	public bool fixedOnBoss;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if(Game.Data.cameraFollowing && !fixedOnBoss) {
			Vector3 pos = transform.position;
			pos = new Vector3(pos.x + (Game.Data.HSpeed * (Game.Data.HSpeed/8f) * Time.deltaTime), pos.y, pos.z);
			transform.position = pos;
		}
		if(this.gameObject.transform.position.x > 90) {
			Destroy(this.gameObject);
		}
	}
}
