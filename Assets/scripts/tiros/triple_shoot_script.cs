﻿using UnityEngine;
using System.Collections;

public class triple_shoot_script : MonoBehaviour {

	public GameObject normal_tiro;

	public int level;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void spawnChilds() {
		level = 2;
		switch(level) {
		case 1:
			Game.spawnShoot(normal_tiro,Game.Data.Rushell.Bazooka.transform.position, Game.Data.angle - 2);
			Game.spawnShoot(normal_tiro,Game.Data.Rushell.Bazooka.transform.position, Game.Data.angle + 2);
			break;
		case 2:
			Game.spawnShoot(normal_tiro,Game.Data.Rushell.Bazooka.transform.position);
			Game.spawnShoot(normal_tiro,Game.Data.Rushell.Bazooka.transform.position, Game.Data.angle - 8);
			Game.spawnShoot(normal_tiro,Game.Data.Rushell.Bazooka.transform.position, Game.Data.angle + 8);
			break;
		case 3:
			Game.spawnShoot(normal_tiro,Game.Data.Rushell.Bazooka.transform.position, Game.Data.angle - 5);
			Game.spawnShoot(normal_tiro,Game.Data.Rushell.Bazooka.transform.position, Game.Data.angle - 2);
			Game.spawnShoot(normal_tiro,Game.Data.Rushell.Bazooka.transform.position, Game.Data.angle + 2);
			Game.spawnShoot(normal_tiro,Game.Data.Rushell.Bazooka.transform.position, Game.Data.angle + 5);
			break;
		case 4:
			Game.spawnShoot(normal_tiro,Game.Data.Rushell.Bazooka.transform.position);
			Game.spawnShoot(normal_tiro,Game.Data.Rushell.Bazooka.transform.position, Game.Data.angle - 4);
			Game.spawnShoot(normal_tiro,Game.Data.Rushell.Bazooka.transform.position, Game.Data.angle - 8);
			Game.spawnShoot(normal_tiro,Game.Data.Rushell.Bazooka.transform.position, Game.Data.angle + 4);
			Game.spawnShoot(normal_tiro,Game.Data.Rushell.Bazooka.transform.position, Game.Data.angle + 8);
			break;
		}
	}
}
