﻿using UnityEngine;
using System.Collections;

public class boom_script : MonoBehaviour {

	public GameObject ExplosionParticle;

	// Use this for initialization
	void Start () {
		if(Game.Data.onBoss) {
			this.gameObject.GetComponent<MoveWithCam>().enabled = false;
		}
		Instantiate(ExplosionParticle, this.transform.position, Quaternion.identity);
		Destroy(this.gameObject, 0.1f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnTriggerEnter2D(Collider2D coll) {
		if(coll.gameObject.tag == "golden_barrel"){
			Debug.Log("Eu matei o golden");
			Stars_Rank_Script.Data.GoldBarrelsExploded++;
			Destroy(coll.gameObject);
		}
	}
}
