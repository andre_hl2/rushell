﻿using UnityEngine;
using System.Collections;

public class tornado_script : MonoBehaviour {

	public int level;
	public bool exploded;

	public int distance1;
	public int distance2;
	public int distance3;
	public int distance4;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(exploded) {
			Collider2D[] res = Physics2D.OverlapAreaAll( new Vector2(this.transform.position.x - distance1, this.transform.position.y + 100), new Vector2(this.transform.position.x + 3, this.transform.position.y - 100));

			switch(level) {
			case 1:
				res = Physics2D.OverlapAreaAll( new Vector2(this.transform.position.x - distance1, this.transform.position.y + 100), new Vector2(this.transform.position.x + 3, this.transform.position.y - 100));
				break;
			case 2:
				res = Physics2D.OverlapAreaAll( new Vector2(this.transform.position.x - distance2, this.transform.position.y + 100), new Vector2(this.transform.position.x + 3, this.transform.position.y - 100));
				break;
			case 3:
				res = Physics2D.OverlapAreaAll( new Vector2(this.transform.position.x - distance3, this.transform.position.y + 100), new Vector2(this.transform.position.x + 3, this.transform.position.y - 100));
				break;
			default:
				res = Physics2D.OverlapAreaAll( new Vector2(this.transform.position.x - distance4, this.transform.position.y + 100), new Vector2(this.transform.position.x + 3, this.transform.position.y - 100));
				break;
			}

			foreach( Collider2D coll in res) {

				if(coll.gameObject.tag == "plataforma" || coll.gameObject.tag == "inimigo" || coll.gameObject.tag == "bronken_plataform") {
					Rigidbody2D rig = coll.gameObject.GetComponent<Rigidbody2D>();
					if(rig != null) {
						rig.velocity = new Vector2(0,0);
						rig.mass = 1;
						rig.gravityScale = 0;
						rig.velocity = new Vector2(-5,8);
						rig.angularVelocity = 100;
					}
				}
				if(coll.gameObject.tag == "plataforma") {
					breakable_plataform_script bre = coll.gameObject.GetComponent<breakable_plataform_script>();
					if(bre != null) {
						bre.Break();
					}
				} else if (coll.gameObject.tag == "inimigo") {
					Destroy(coll.gameObject, 2.0f);
				}
			}
		}
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if(coll.gameObject.tag == "plataforma") {
			breakable_plataform_script bre = coll.gameObject.GetComponent<breakable_plataform_script>();
			if(bre != null)
				bre.Break();
			exploded = true;
			this.gameObject.GetComponent<Rigidbody2D>().isKinematic = true;
			this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
			this.gameObject.GetComponent<Rigidbody2D>().AddTorque(500);
			Destroy(this.gameObject,5);
		}
		if(coll.gameObject.tag == "ground") {
			exploded = true;
			this.gameObject.GetComponent<Rigidbody2D>().isKinematic = true;
			this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
			Destroy(this.gameObject,5);
		}
		
		
	}
}
