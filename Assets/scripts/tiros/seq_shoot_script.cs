﻿using UnityEngine;
using System.Collections;

public class seq_shoot_script : MonoBehaviour {

	public GameObject normal_shoot;

	public int level;

	public float time1;
	public float time2;
	public float time3;
	public float time4;

	public float angle;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void Shoot() {
		angle = Game.Data.angle;
		Game.spawnShoot(normal_shoot,Game.Data.Rushell.Bazooka.transform.position,angle);

		level = 3;

		Invoke("Spawn",time1);
		if(level > 1)
			Invoke("Spawn",time2);
		if(level > 2)
			Invoke("Spawn",time3);
		if(level > 3)
			Invoke("Spawn",time4);

		Destroy(this.gameObject,time4+ 0.01f);
	}

	public void Spawn() {
		Game.spawnShoot(normal_shoot,Game.Data.Rushell.Bazooka.transform.position,angle);
	}
}
