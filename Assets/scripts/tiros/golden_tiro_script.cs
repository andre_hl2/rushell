﻿using UnityEngine;
using System.Collections;

public class golden_tiro_script : MonoBehaviour {

	public int level;

	public GameObject gold_holder1;
	public GameObject gold_holder2;
	public GameObject gold_holder3;
	public GameObject gold_holder4;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if(coll.gameObject.tag == "plataforma") {
			breakable_plataform_script bre = coll.gameObject.GetComponent<breakable_plataform_script>();
			if(bre != null)
				bre.Break();
			if(level == 4)
				Instantiate(gold_holder2,coll.gameObject.transform.position,Quaternion.identity);
			else
				Instantiate(gold_holder1,coll.gameObject.transform.position,Quaternion.identity);
			Destroy(this.gameObject);
		}
		if(coll.gameObject.tag == "ground") {
			if(level == 4)
				Instantiate(gold_holder2,this.transform.position,Quaternion.identity);
			else
				Instantiate(gold_holder1,this.transform.position,Quaternion.identity);
			Destroy(this.gameObject);
		}

		if(coll.gameObject.tag == "inimigo") {
			switch(level) {
			case 1:
				Instantiate(gold_holder1,coll.gameObject.transform.position,Quaternion.identity);
				break;
			case 2:
				Instantiate(gold_holder2,coll.gameObject.transform.position,Quaternion.identity);
				break;
			case 3:
				Instantiate(gold_holder3,coll.gameObject.transform.position,Quaternion.identity);
				break;
			case 4:
				Instantiate(gold_holder4,coll.gameObject.transform.position,Quaternion.identity);
				break;
			}
		}
	}
}
