﻿using UnityEngine;
using System.Collections;

public class missile_tiro_script : MonoBehaviour {

	public bool pursuing;
	public float speed;
	public GameObject target;

	public float radius;
	// Use this for initialization
	void Start () {
		pursuing = false;
	}
	
	// Update is called once per frame
	void Update () {
		Vector2 dir;
		float angle;

		if(!pursuing) {
			Collider2D[] res = Physics2D.OverlapCircleAll(this.transform.position, radius);
			foreach (Collider2D coll in res) {
				if(coll.gameObject.tag == "inimigo") {
					target = coll.gameObject;
					pursuing = true;
				}
			}
			dir = this.gameObject.GetComponent<Rigidbody2D>().velocity;
			angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
			this.transform.rotation = Quaternion.AngleAxis(angle, new Vector3(0,0,1));
		} else {
			if(target != null) {
				dir = target.transform.position - transform.position;
				angle = Mathf.Atan2(dir.y,dir.x) * Mathf.Rad2Deg;
				transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

				this.GetComponent<Rigidbody2D>().velocity = dir * speed;
			}
			dir = this.gameObject.GetComponent<Rigidbody2D>().velocity;
			angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
			this.transform.rotation = Quaternion.AngleAxis(angle, new Vector3(0,0,1));
		}
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if(coll.gameObject.tag == "plataforma") {
			breakable_plataform_script bre = coll.gameObject.GetComponent<breakable_plataform_script>();
			if(bre != null)
				bre.Break();
			Destroy(this.gameObject);
		}
		if(coll.gameObject.tag == "ground") {
			Destroy(this.gameObject);
		}
	}
}
