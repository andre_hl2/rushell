﻿using UnityEngine;
using System.Collections;

public class black_hole_script : MonoBehaviour {

	public bool exploded;

	public int level;

	public int radius1;
	public int radius2;
	public int radius3;
	public int radius4;

	public ParticleSystem particle;

	public AudioClip blackHoleSound;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {

		this.gameObject.GetComponent<Rigidbody2D>().angularVelocity = -800;

		if(exploded) {
			//this.gameObject.GetComponent<MoveWithCam>().enabled = false;

			Collider2D[] res = Physics2D.OverlapCircleAll(this.transform.position, radius1);

			switch(level) {
			case 1:
				res = Physics2D.OverlapCircleAll(this.transform.position,radius3);
				break;
			case 2:
				res = Physics2D.OverlapCircleAll(this.transform.position,radius3);
				break;
			case 3:
				res = Physics2D.OverlapCircleAll(this.transform.position,radius3);
				break;
			case 4:
				res = Physics2D.OverlapCircleAll(this.transform.position,radius3);
				break;
			default:
				break;
			}

			foreach( Collider2D coll in res) {

				if(coll.gameObject.tag == "plataforma" || coll.gameObject.tag == "inimigo" || coll.gameObject.tag == "bronken_plataform") {
					Rigidbody2D rig = coll.gameObject.GetComponent<Rigidbody2D>();
					if(rig != null) {
						rig.velocity = new Vector2(0,0);
						rig.mass = 1;
						rig.gravityScale = 0;
						Vector2 dir = this.transform.position - coll.gameObject.transform.position;
						dir.Normalize();
						dir *= 200;
						rig.AddForce(dir);
					}
				}
				if(coll.gameObject.tag == "plataforma") {
					breakable_plataform_script bre = coll.gameObject.GetComponent<breakable_plataform_script>();
					if(bre != null) {
						bre.Break();
					}
				} else if (coll.gameObject.tag == "inimigo") {

				}
			}

		}
	
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if(Game.Data.onBoss) {
			this.gameObject.GetComponent<MoveWithCam>().enabled = false;
		}
		if(coll.gameObject.tag == "plataforma") {
			breakable_plataform_script bre = coll.gameObject.GetComponent<breakable_plataform_script>();
			if(bre != null)
				bre.Break();
			exploded = true;
			particle.enableEmission = true;
			this.gameObject.GetComponent<Rigidbody2D>().isKinematic = true;
			this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
			AudioSource.PlayClipAtPoint(blackHoleSound,Camera.main.transform.position);
			Destroy(this.gameObject,5);
		}
		if(coll.gameObject.tag == "ground") {
			exploded = true;
			particle.enableEmission = true;
			this.gameObject.GetComponent<Rigidbody2D>().isKinematic = true;
			this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
			AudioSource.PlayClipAtPoint(blackHoleSound,Camera.main.transform.position);
			Destroy(this.gameObject,5);
		}


	}

	void OnTriggerStay2D(Collider2D coll) {
		if(coll.gameObject.tag == "inimigo" || coll.gameObject.tag == "bronken_plataform") {
			Destroy(coll.gameObject);
		}
		if(coll.gameObject.GetComponent<espinhos_script>() != null) {
			Destroy(coll.gameObject);
		}
	}
}
