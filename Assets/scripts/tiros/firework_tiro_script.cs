﻿using UnityEngine;
using System.Collections;

public class firework_tiro_script : MonoBehaviour {

	public int level;

	public float radius1;
	public float radius2;
	public float radius3;
	public float radius4;

	public GameObject firework1;
	public GameObject firework2;
	public GameObject firework3;
	public GameObject firework4;

	public Vector3 offset2;
	public Vector3 offset3;
	public Vector3 offset4;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector2 dir = this.gameObject.GetComponent<Rigidbody2D>().velocity;
		float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
		this.transform.rotation = Quaternion.AngleAxis(angle, new Vector3(0,0,1));

	}

	void OnTriggerEnter2D(Collider2D coll) {
		if(coll.gameObject.tag == "plataforma" || coll.gameObject.tag == "ground" || coll.gameObject.tag == "inimigo") {

			Collider2D[] res = Physics2D.OverlapCircleAll(this.transform.position,radius1);
			switch(level) {
			case 1:
				res = Physics2D.OverlapCircleAll(this.transform.position,radius3);
				break;
			case 2:
				res = Physics2D.OverlapCircleAll(this.transform.position,radius3);
				break;
			case 3:
				res = Physics2D.OverlapCircleAll(this.transform.position,radius3);
				break;
			case 4:
				res = Physics2D.OverlapCircleAll(this.transform.position,radius3);
				break;
			}

			foreach(Collider2D col in res) {
				if(col.gameObject.tag == "plataforma") {
					breakable_plataform_script bre = col.gameObject.GetComponent<breakable_plataform_script>();
					if(bre != null) {
						bre.Break();
					}
				} else if (col.gameObject.tag == "inimigo") {
					// FAZER SPAWNAR OUTRO FIREWORK AQUI
					Destroy(col.gameObject);
				}
			}

			Instantiate(firework1, this.transform.position, Quaternion.identity);

			if(level > 1) {
				Instantiate(firework2, this.transform.position + offset2, Quaternion.identity);
			}
			if(level > 2) {
				Instantiate(firework3, this.transform.position + offset3, Quaternion.identity);
			}
			if(level > 3) {
				Instantiate(firework4, this.transform.position + offset4, Quaternion.identity);
			}

			Destroy(this.gameObject);
		}
	}
}
