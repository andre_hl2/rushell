﻿using UnityEngine;
using System.Collections;

public class homing_shot_script : MonoBehaviour {

	public GameObject missile_shoot;
	public int level;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void spawnChilds() {
		level = 1;
		switch(level) {
		case 1:
			Game.spawnShoot(missile_shoot,Game.Data.Rushell.Bazooka.transform.position);
			break;
		case 2:
			Game.spawnShoot(missile_shoot,Game.Data.Rushell.Bazooka.transform.position, Game.Data.angle - 3);
			Game.spawnShoot(missile_shoot,Game.Data.Rushell.Bazooka.transform.position, Game.Data.angle + 3);
			break;
		case 3:
			Game.spawnShoot(missile_shoot,Game.Data.Rushell.Bazooka.transform.position);
			Game.spawnShoot(missile_shoot,Game.Data.Rushell.Bazooka.transform.position, Game.Data.angle - 5);
			Game.spawnShoot(missile_shoot,Game.Data.Rushell.Bazooka.transform.position, Game.Data.angle + 5);
			break;
		case 4:
			Game.spawnShoot(missile_shoot,Game.Data.Rushell.Bazooka.transform.position, Game.Data.angle - 2.5f);
			Game.spawnShoot(missile_shoot,Game.Data.Rushell.Bazooka.transform.position, Game.Data.angle - 7.5f);
			Game.spawnShoot(missile_shoot,Game.Data.Rushell.Bazooka.transform.position, Game.Data.angle + 2.5f);
			Game.spawnShoot(missile_shoot,Game.Data.Rushell.Bazooka.transform.position, Game.Data.angle + 7.5f);
			break;
		}
	}
}
