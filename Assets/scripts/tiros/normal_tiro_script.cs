﻿using UnityEngine;
using System.Collections;

public class normal_tiro_script : MonoBehaviour {

	public AudioClip normal_sound;
	public GameObject boom;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if(coll.gameObject.tag == "plataforma") {
			if(SoundController.Data != null)
				if(!SoundController.Data.mutedSFX)
				SoundController.Data.AudioSources["normal_shoot"].Play();

			breakable_plataform_script bre = coll.gameObject.GetComponent<breakable_plataform_script>();
			if(bre != null)
				bre.Break();
			Instantiate(boom, this.transform.position, Quaternion.identity);
			Destroy(this.gameObject);
		}
		if(coll.gameObject.tag == "ground") {
			if(SoundController.Data != null)
				if(!SoundController.Data.mutedSFX)
					SoundController.Data.AudioSources["normal_shoot"].Play();

			Instantiate(boom, this.transform.position, Quaternion.identity);
			Destroy(this.gameObject);
		}
		if(coll.gameObject.tag == "inimigo" || coll.gameObject.tag == "Boss") {
			Instantiate(boom, this.transform.position, Quaternion.identity);
		}
	}
}
