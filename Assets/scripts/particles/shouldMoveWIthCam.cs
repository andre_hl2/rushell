﻿using UnityEngine;
using System.Collections;

public class shouldMoveWIthCam : MonoBehaviour {

	// Use this for initialization
	void Start () {
		bool should = Game.Data != null;
		if(should) {
			should = !Game.Data.onBoss;
		}
		MoveWithCam mwc = this.gameObject.GetComponent<MoveWithCam>();
		if(!should) {
			if(mwc != null)
				mwc.enabled = false;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
