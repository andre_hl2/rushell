﻿using UnityEngine;
using System.Collections;

public class destroy_out_screen : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Destroy(this.gameObject, 5);
	}
	
	// Update is called once per frame
	void Update () {
		if(this.gameObject.transform.position.x > 20) {
			Destroy(this.gameObject);
		}
	}
}
