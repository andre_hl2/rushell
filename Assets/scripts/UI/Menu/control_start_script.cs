﻿using UnityEngine;
using System.Collections;

public class control_start_script : MonoBehaviour {

	public AudioClip rushell;

	public GameObject loading;

	// Use this for initialization
	void Start () {
		loading.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void StarLoad() {
		loading.SetActive(true);
		if(!SoundController.Data.mutedSFX) {
			SoundController.Data.AudioSources["rushell"].Play();
		}
	}
}
