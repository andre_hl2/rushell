﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class total_coin_script : MonoBehaviour {

	public Text txt;
	public int coins;

	// Use this for initialization
	void Start () {
		txt = this.gameObject.GetComponent<Text>();

		if(PlayerPrefs.HasKey("coins"))
			coins = PlayerPrefs.GetInt("coins");
		else
			coins = 0;

		if(txt != null)
			txt.text = coins.ToString();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
