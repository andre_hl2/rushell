﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PinchZoom_UI : MonoBehaviour {

	public float zoomSpeed = 0.5f;
	public GameObject objectToZoom;

	void Update() {
		if(Input.touchCount == 2) {
			ScrollRect rect = objectToZoom.GetComponent<ScrollRect>();
			if(rect != null) {
				rect.enabled = false;
			}

			Touch touchZero = Input.GetTouch(0);
			Touch touchOne  = Input.GetTouch(1);

			Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
			Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

			float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
			float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

			float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

			float scale = objectToZoom.transform.localScale.x;

			scale += deltaMagnitudeDiff * zoomSpeed;

			scale = Mathf.Clamp(scale,0.4f, 1.3f);

			objectToZoom.transform.localScale = new Vector3(scale,scale,1);

		} else {
			ScrollRect rect = objectToZoom.GetComponent<ScrollRect>();
			if(rect != null) {
				rect.enabled = true;
			}
		}
	}
}
