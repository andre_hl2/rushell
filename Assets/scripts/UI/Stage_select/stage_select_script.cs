﻿using UnityEngine;
using System.Collections;

public class stage_select_script : MonoBehaviour {


	// Use this for initialization
	void Start () {
		if(SoundController.Data != null) {
			SoundController.Data.AudioSources["rain"].Pause();
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void beginStage(){
		PlayerPrefs.SetInt("numberOfLevel", StageSelect.Data.numberOfLevelSelected);
		PlayerPrefs.SetInt("difficult", StageSelect.Data.difficult);
		PlayerPrefs.SetString("nameOfLevel", StageSelect.Data.nameOfLevelSelected);
		PlayerPrefs.Save();
		StageSelect.Data.Loading = true;
		Application.LoadLevelAsync(StageSelect.Data.nameOfLevelSelected);
		//Application.LoadLevel(StageSelect.Data.nameOfLevelSelected);
	}

	public void gotoShop() {
		Application.LoadLevel("cena_loja");
	}

	public void Clear() {
		StageSelect.Data.levelsClearTemple   = 0;
		StageSelect.Data.levelsClearUnicorn  = 0;
		StageSelect.Data.levelsClearCanabalt = 0;

		PlayerPrefs.SetInt("levelsClearTemple"  ,0);
		PlayerPrefs.SetInt("levelsClearUnicorn" ,0);
		PlayerPrefs.SetInt("levelsClearCanabalt",0);

		PlayerPrefs.Save();
	}

	public void Cancel() {
		StageSelect.Data.selected = false;
	}

	public void CancelLocked() {
		StageSelect.Data.locked = false;
	}
}
