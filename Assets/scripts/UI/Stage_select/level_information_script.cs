﻿using UnityEngine;
using UnityEngine.UI;

public enum World { Temple, Unicorn, Canabalt };

public class level_information_script : MonoBehaviour {

	public bool locked;
	public bool Boss;
	public World world;
	public int number;
	public int difficult;
	public string nameOfLevel;
	public int stars;
	public GameObject level_button;

	public Text level_button_text;
	public Image star1;
	public Image star2;
	public Image star3;

	public Sprite emptyRock;

	string initialText;
	public string nameToShow;

	public Sprite Star1Filled;
	public Sprite Star1Empty;
	public Sprite Star2Filled;
	public Sprite Star2Empty;
	public Sprite Star3Filled;
	public Sprite Star3Empty;

	public Text info_nameToShowOnModal;
	public Image info_star1;
	public Image info_star2;
	public Image info_star3;

	int levelsClear;

	// Use this for initialization
	void Start () {
		initialText = level_button_text.text;
		string key = nameOfLevel+"_stars_"+number.ToString();
		if(PlayerPrefs.HasKey(key))
			stars = PlayerPrefs.GetInt(key);
		else
			stars = 0;

		switch(world) {
		case World.Temple:
			levelsClear = StageSelect.Data.levelsClearTemple;
			break;
		case World.Unicorn:
			levelsClear = StageSelect.Data.levelsClearUnicorn;
			break;
		case World.Canabalt:
			levelsClear = StageSelect.Data.levelsClearCanabalt;
			break;
		}
	}
	
	// Update is called once per frame
	void Update () {
		Button btn = level_button.GetComponent<Button>();
		Image img = level_button.GetComponent<Image>();

		if(levelsClear >= number - 1) {
			if(btn != null) {
				btn.enabled = true;
			}
			if(img != null) {
				img.color = new Color(img.color.r, img.color.g, img.color.b, 1.0f);
			}

			if(!Boss) {
				if(star1 != null && star2 != null && star3 != null){
					star1.enabled = true;
					star2.enabled = true;
					star3.enabled = true;
					
					if(stars >= 1)
						star1.sprite = Star2Filled;
					else
						star1.sprite = Star2Empty;
					if(stars >= 2)
						star2.sprite = Star2Filled;
					else
						star2.sprite = Star2Empty;
					if(stars >= 3)
						star3.sprite = Star2Filled;
					else
						star3.sprite = Star2Empty;
				}
			}

			level_button_text.text = initialText;
		} else {


			if(btn != null) {
				btn.enabled = false;
			}
			if(img != null) {
				//img.color = new Color(img.color.r, img.color.g, img.color.b, 0.7f);
				img.sprite = emptyRock;
			}
			level_button_text.text = "?";

			if(!Boss) {
				if(star1 != null && star2 != null && star3 != null){
					star1.enabled = false;
					star2.enabled = false;
					star3.enabled = false;
				}
			}
		}
	}

	public void chooseLevel() {
		if(locked) {
			info_nameToShowOnModal.text = "Coming Soon";
			StageSelect.Data.locked = true;
		} else {
			StageSelect.Data.difficult = difficult;
			StageSelect.Data.numberOfLevelSelected = number;
			StageSelect.Data.nameOfLevelSelected = nameOfLevel;
			info_nameToShowOnModal.text = nameToShow;
			StageSelect.Data.selected = true;
			
			if(!Boss) {
				info_star1.sprite = Star1Empty;
				info_star2.sprite = Star2Empty;
				info_star3.sprite = Star3Empty;
				
				if(stars >= 1)
					info_star1.sprite = Star1Filled;
				if(stars >= 2)
					info_star2.sprite = Star2Filled;
				if(stars >= 3)
					info_star3.sprite = Star3Filled;
			}
		}

	}
}
