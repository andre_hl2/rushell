﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class black_image : MonoBehaviour {

	Image img;

	// Use this for initialization
	void Start () {
		img = this.gameObject.GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
		if(img != null) {
			img.color = new Color(0,0,0,1);
		}
	}
}
