﻿using UnityEngine;
using UnityEngine.UI;

public class StageSelect : MonoBehaviour {

	public static StageSelect Data;
	public int levelsClearTemple;
	public int levelsClearUnicorn;
	public int levelsClearCanabalt;

	public bool Loading = false;

	public bool levelSelected;
	public int difficult;
	public string nameOfLevelSelected;
	public int numberOfLevelSelected;

	public bool selected;
	public bool locked;

	// Use this for initialization
	void Start () {

		QualitySettings.vSyncCount = 0;
		Application.targetFrameRate =  60;

		if(PlayerPrefs.HasKey("numberOfLevel") && PlayerPrefs.HasKey("difficult") && PlayerPrefs.HasKey("nameOfLevel")) {
			numberOfLevelSelected = PlayerPrefs.GetInt("numberOfLevel");
			difficult = PlayerPrefs.GetInt("difficult");
			nameOfLevelSelected = PlayerPrefs.GetString("nameOfLevel");
		} else {
			numberOfLevelSelected = 1;
			difficult = 1;
			nameOfLevelSelected = "cena_temple";
		}
	}

	void Awake() {
		if (Data == null) {
			Data = this;
			if(PlayerPrefs.HasKey("levelsClearTemple"))
				StageSelect.Data.levelsClearTemple = PlayerPrefs.GetInt("levelsClearTemple");
			else
				StageSelect.Data.levelsClearTemple = 0;
			
			if(PlayerPrefs.HasKey("levelsClearUnicorn"))
				StageSelect.Data.levelsClearUnicorn = PlayerPrefs.GetInt("levelsClearUnicorn");
			else
				StageSelect.Data.levelsClearUnicorn = 0;

			if(PlayerPrefs.HasKey("levelsClearCanabalt"))
				StageSelect.Data.levelsClearCanabalt = PlayerPrefs.GetInt("levelsClearCanabalt");
			else
				StageSelect.Data.levelsClearCanabalt = 0;
		}
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void unlockAll() {
		levelsClearTemple   = 15;
		levelsClearUnicorn  = 15;
		levelsClearCanabalt = 15;

		PlayerPrefs.SetInt("levelsClearTemple",15);
		PlayerPrefs.SetInt("levelsClearUnicorn",15);
		PlayerPrefs.SetInt("levelsClearCanabalt",15);
		PlayerPrefs.Save();
	}
	public void lockAll() {
		levelsClearTemple   = 0;
		levelsClearUnicorn  = 0;
		levelsClearCanabalt = 0;

		PlayerPrefs.SetInt("levelsClear",0);
		PlayerPrefs.SetInt("coins",0);

		for(int i=0;i<15;i++){
			string key = "cena_temple_stars_"+i.ToString();
			PlayerPrefs.SetInt(key,0);
			key = "cena_unicorn_stars_"+i.ToString();
			PlayerPrefs.SetInt(key,0);
			key = "cena_canabalt_stars_"+i.ToString();
			PlayerPrefs.SetInt(key, 0);
		}
		PlayerPrefs.SetInt("levelsClearTemple",0);
		PlayerPrefs.SetInt("levelsClearUnicorn",0);
		PlayerPrefs.SetInt("levelsClearCanabalt",0);

		PlayerPrefs.Save();
	}

	public void BackMenu() {
		Application.LoadLevel("cena_menu");
	}
}
