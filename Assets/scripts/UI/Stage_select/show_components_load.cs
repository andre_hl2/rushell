﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class show_components_load : MonoBehaviour {

	public Image img;
	public Button btn;
	public Text txt;

	// Use this for initialization
	void Start () {
		img = this.gameObject.GetComponent<Image>();
		txt = this.gameObject.GetComponent<Text>();
		btn = this.gameObject.GetComponent<Button>();
	}
	
	// Update is called once per frame
	void Update () {
		if(img != null)
			img.enabled = StageSelect.Data.Loading;
		if(txt != null)
			txt.enabled = StageSelect.Data.Loading;
		if(btn != null)
			btn.enabled = StageSelect.Data.Loading;
	}
}
