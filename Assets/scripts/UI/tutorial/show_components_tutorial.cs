﻿using UnityEngine;
using UnityEngine.UI;

public class show_components_tutorial : MonoBehaviour {


	public bool checkNumber;
	public int numberOfTutorial;

	Button btn;
	Image img;
	Text txt;

	// Use this for initialization
	void Start () {
		btn = this.gameObject.GetComponent<Button>();
		img = this.gameObject.GetComponent<Image>();
		txt = this.gameObject.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		bool show = false;


		if(Tutorial.Data.showing) {
			if(checkNumber)
				show = (Tutorial.Data.numberOfTutorial == numberOfTutorial);
			else
				show = true;
		}

		if(btn != null)
			btn.enabled = show;
		if(img != null)
			img.enabled = show;
		if(txt != null)
			txt.enabled = show;
	}
}
