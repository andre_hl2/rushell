﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class Tutorial : MonoBehaviour {

	public static Tutorial Data;

	public bool showing;
	public int numberOfTutorial;

	BlurOptimized blur;

	public void Awake() {
		if (Data == null) {
			Data = this;
		}
	}

	// Use this for initialization
	void Start () {
		blur = Camera.main.GetComponent<BlurOptimized>();
		blur.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		blur.enabled = showing;
		Game.Data.Rushell.Animator.enabled = !showing;
		if(showing || Game.Data.Paused) {
			Time.timeScale = 0;
		} else {
			Time.timeScale = 1;
		}
	}

	public void OkButton() {
		showing = false;
		numberOfTutorial++;
	}
}
