﻿using UnityEngine;
using UnityEngine.UI;

public class coin_counter : MonoBehaviour {

	public Text coins;
	public int total_coins;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		coins.text = Stars_Rank_Script.Data.roundCoins+"/"+Stars_Rank_Script.Data.desiredCoins;
	}
}
