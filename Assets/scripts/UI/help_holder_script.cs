﻿using UnityEngine;
using System.Collections;

public class help_holder_script : MonoBehaviour {

	public helper_script help;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if(coll.gameObject.tag == "Player"){
			if(Game.Data.actualLevel == 1) {
				if(!PlayerPrefs.HasKey("jaMostrouHelp")) {
					help.ShowHelp();
					PlayerPrefs.SetInt("jaMostrouHelp",1);
					PlayerPrefs.Save();
				}
			}
		}
	}
}
