﻿using UnityEngine;
using UnityEngine.UI;

public class tab_font_Script : MonoBehaviour {


	public int myTabNumber;
	public bool subTab;
	Text txt;

	// Use this for initialization
	void Start () {
		txt = this.gameObject.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		Color col = txt.color;
		if(subTab) {
			if(GameSkill.Data.subTabSeleted == myTabNumber)
				col.a = 1;
			else
			col.a = 0.5f;
		} else {
			if(GameSkill.Data.tabSelected == myTabNumber)
				col.a = 1;
			else
				col.a = 0.5f;
		}
		
		txt.color = col;
	}
}
