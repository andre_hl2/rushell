﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class showComponents_subtab : MonoBehaviour {

	public int tab;
	public int subtab;

	Image img;

	// Use this for initialization
	void Start () {
		img = this.gameObject.GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
		if(img != null) {
			bool show = (GameSkill.Data.tabSelected == tab && GameSkill.Data.subTabSeleted == subtab && GameSkill.Data.showingShop);
			img.enabled = show;
		}
	}
}
