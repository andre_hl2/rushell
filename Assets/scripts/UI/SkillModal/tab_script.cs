﻿using UnityEngine;
using UnityEngine.UI;

public class tab_script : MonoBehaviour {
	
	public int myTabNumber;
	public bool subtab;
	public Sprite Open;
	public Sprite Closed;

	public Image img;

	// Use this for initialization
	void Start () {
		img = this.gameObject.GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
		if(subtab) {
			if(GameSkill.Data.subTabSeleted == myTabNumber)
				img.sprite = Open;
			else
				img.sprite = Closed;
		} else {
			if(GameSkill.Data.tabSelected == myTabNumber)
				img.sprite = Open;
			else
				img.sprite = Closed;
		}

	}
}
