﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShowComponent_tabContent : MonoBehaviour {

	public int myTabNumber;
	Button btn;
	Image img;
	Text txt;
	
	// Use this for initialization
	void Start () {
		btn = this.gameObject.GetComponent<Button>();
		img = this.gameObject.GetComponent<Image>();
		txt = this.gameObject.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		bool show = ( GameSkill.Data.showingShop && (GameSkill.Data.tabSelected == myTabNumber) );
		
		if(btn != null)
			btn.enabled = show;
		if(img != null)
			img.enabled = show;
		if(txt != null)
			txt.enabled = show;
	}
}
