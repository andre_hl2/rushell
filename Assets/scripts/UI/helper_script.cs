﻿using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class helper_script : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void ShowHelp() {
		Game.Data.showingHelp = true;
		Time.timeScale = 0;
		Camera.main.GetComponent<BlurOptimized>().enabled = true;
		Game.Data.Rushell.Animator.enabled = false;
	}

	public void OK() {
		Game.Data.showingHelp = false;
		Time.timeScale = 1;
		Camera.main.GetComponent<BlurOptimized>().enabled = false;
		Game.Data.Rushell.Animator.enabled = true;
	}
}
