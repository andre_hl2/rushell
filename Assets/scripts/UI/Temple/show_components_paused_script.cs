﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class show_components_paused_script : MonoBehaviour {

	public bool showOnPause;
	public bool hideOnStageClear;

	public Text txt;
	public Button btn;
	public Image img;

	// Use this for initialization
	void Start () {
		txt = this.gameObject.GetComponent<Text>();
		btn = this.gameObject.GetComponent<Button>();
		img = this.gameObject.GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
		bool show = false;

		if(Game.Data.Paused) {
			if(showOnPause)
				show = true;
			else
				show = false;
		} else {
			if(showOnPause)
				show = false;
			else
				show = true;
		}

		if(txt != null)
			txt.enabled = show;
		if(btn != null)
			btn.enabled = show;
		if(img != null)
			img.enabled = show;
	}
}
