﻿using UnityEngine;
using UnityEngine.UI;

public class show_components_terminated_script : MonoBehaviour {

	Button btn;
	Image img;
	Text txt;

	// Use this for initialization
	void Start () {
		btn = this.gameObject.GetComponent<Button>();
		img = this.gameObject.GetComponent<Image>();
		txt = this.gameObject.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		bool show = Game.Data.levelTerminated;
		if(btn != null)
			btn.enabled = show;
		if(img != null)
			img.enabled = show;
		if(txt != null)
			txt.enabled = show;
	}
}
