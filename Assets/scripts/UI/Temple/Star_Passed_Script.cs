﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Star_Passed_Script : MonoBehaviour {

	public Image star;

	public int myNumber;
	public Sprite EmptyStar;
	public Sprite FilledStar;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Game.Data.passedLevel) {
			if(Stars_Rank_Script.Data.getStars() >= myNumber)
				star.sprite = FilledStar;
			else
				star.sprite = EmptyStar;
		}
	}
}
