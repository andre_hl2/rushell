﻿using UnityEngine;
using System.Collections;

public class play_effects_script : MonoBehaviour {

	public AudioSource src;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Play_Thunder() {
		if(SoundController.Data != null)
			if(!SoundController.Data.mutedSFX)
				SoundController.Data.AudioSources["thunder_sound"].Play();
	}
}
