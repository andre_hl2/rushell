﻿using UnityEngine;
using System.Collections;

public class count_coins_enemies_script : MonoBehaviour {

	public int coin_number;
	public int enemy_number;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Count() {
		coin_script[] res = this.gameObject.GetComponentsInChildren<coin_script>();
		coin_number = res.Length;


		aranha_script[] res1 = this.gameObject.GetComponentsInChildren<aranha_script>();
		escorpiao_script[] res2 = this.gameObject.GetComponentsInChildren<escorpiao_script>();
		guerreiro_script[] res3 = this.gameObject.GetComponentsInChildren<guerreiro_script>();
		cavalo_marinho_script[] res4 = this.gameObject.GetComponentsInChildren<cavalo_marinho_script>();
		gigante_script[] res5 = this.gameObject.GetComponentsInChildren<gigante_script>();
		fada_fogo_script[] res6 = this.gameObject.GetComponentsInChildren<fada_fogo_script>();

		enemy_number = res1.Length + res2.Length + res3.Length + res4.Length + res5.Length + res6.Length;
	}
}
