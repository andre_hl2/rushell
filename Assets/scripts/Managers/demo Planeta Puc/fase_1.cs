﻿using UnityEngine;
using System.Collections;

public class fase_1 : MonoBehaviour {

	//MOEDA
	public GameObject ter_moeda_1;
	bool ja_moeda_1;
	public GameObject ter_moeda_2;
	bool ja_moeda_2;
	public GameObject ter_moeda_3;
	bool ja_moeda_3;

	//CENARIO
	public GameObject ter_scenario_1;
	bool ja_sce_1;
	public GameObject ter_scenario_2;
	bool ja_sce_2;
	public GameObject ter_scenario_3;
	bool ja_sce_3;
	public GameObject ter_scenario_4;
	bool ja_sce_4;

	//INIMIGO
	public GameObject ter_enemy_1;
	bool ja_ene_1;
	public GameObject ter_enemy_2;
	bool ja_ene_2;
	public GameObject ter_enemy_3;
	bool ja_ene_3;
	public GameObject ter_enemy_4;
	bool ja_ene_4;
	public GameObject ter_enemy_5;
	bool ja_ene_5;

	//CENARIO E MOEDA
	public GameObject ter_sce_coi_1;
	bool ja_sce_coi_1;
	public GameObject ter_sce_coi_2;
	bool ja_sce_coi_2;
	public GameObject ter_sce_coi_3;
	bool ja_sce_coi_3;



	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public GameObject getTerrain(int number) {
		GameObject res;
		if(number == 1 || number == 4 || number == 11) {
			res = createCoin();
		} else if(number == 2 || number == 5 || number == 8 || number == 14) {
			res = createScenario();
		} else if(number == 7 || number == 10 || number == 13) {
			res = createScenarioCoin();
		} else {
			res = createEnemy();
		}
		return res;
	}


	GameObject createCoin() {
		GameObject res = null;
		int ran = 0;
		bool jaCriou = true;
		while(jaCriou) {
			ran = Random.Range(0,3);
			switch(ran) {
			case 0:
				res = ter_moeda_1;
				jaCriou = ja_moeda_1;
				break;
			case 1:
				res = ter_moeda_2;
				jaCriou = ja_moeda_2;
				break;
			case 2:
				res = ter_moeda_3;
				jaCriou = ja_moeda_3;
				break;
			}
		}
		switch(ran) {
		case 0:
			ja_moeda_1 = true;
			break;
		case 1:
			ja_moeda_2 = true;
			break;
		case 2:
			ja_moeda_3 = true;
			break;
		}
		return res;
	}


	GameObject createScenario() {
		GameObject res = null;
		int ran = 0;
		bool jaCriou = true;
		while(jaCriou) {
			ran = Random.Range(0,4);
			switch(ran) {
			case 0:
				res = ter_scenario_1;
				jaCriou = ja_sce_1;
				break;
			case 1:
				res = ter_scenario_2;
				jaCriou = ja_sce_2;
				break;
			case 2:
				res = ter_scenario_3;
				jaCriou = ja_sce_3;
				break;
			case 3:
				res = ter_scenario_4;
				jaCriou = ja_sce_4;
				break;
			}
		}
		switch(ran) {
		case 0:
			ja_sce_1 = true;
			break;
		case 1:
			ja_sce_2 = true;
			break;
		case 2:
			ja_sce_3 = true;
			break;
		case 3:
			ja_sce_4 = true;
			break;
		}
		return res;
	}


	GameObject createEnemy() {
		GameObject res = null;
		int ran = 0;
		bool jaCriou = true;
		while(jaCriou) {
			ran = Random.Range(0,5);
			switch(ran) {
			case 0:
				res = ter_enemy_1;
				jaCriou = ja_ene_1;
				break;
			case 1:
				res = ter_enemy_2;
				jaCriou = ja_ene_2;
				break;
			case 2:
				res = ter_enemy_3;
				jaCriou = ja_ene_3;
				break;
			case 3:
				res = ter_enemy_4;
				jaCriou = ja_ene_4;
				break;
			case 4:
				res = ter_enemy_5;
				jaCriou = ja_ene_5;
				break;
			}
		}
		switch(ran) {
		case 0:
			ja_ene_1 = true;
			break;
		case 1:
			ja_ene_2 = true;
			break;
		case 2:
			ja_ene_3 = true;
			break;
		case 3:
			ja_ene_4 = true;
			break;
		case 4:
			ja_ene_5 = true;
			break;
		}
		return res;
	}


	GameObject createScenarioCoin() {
		GameObject res = null;
		int ran = 0;
		bool jaCriou = true;
		while(jaCriou) {
			ran = Random.Range(0,3);
			switch(ran) {
			case 0:
				res = ter_sce_coi_1;
				jaCriou = ja_sce_coi_1;
				break;
			case 1:
				res = ter_sce_coi_2;
				jaCriou = ja_sce_coi_2;
				break;
			case 2:
				res = ter_sce_coi_3;
				jaCriou = ja_sce_coi_3;
				break;
			}
		}
		switch(ran) {
		case 0:
			ja_sce_coi_1 = true;
			break;
		case 1:
			ja_sce_coi_2 = true;
			break;
		case 2:
			ja_sce_coi_3 = true;
			break;
		}
		return res;
	}
}
