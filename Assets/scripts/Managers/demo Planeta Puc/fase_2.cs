﻿using UnityEngine;
using System.Collections;

public class fase_2 : MonoBehaviour {

	//MOEDA
	public GameObject ter_moeda_1;
	bool ja_moeda_1;
	public GameObject ter_moeda_2;
	bool ja_moeda_2;
	
	//CENARIO
	public GameObject ter_scenario_1;
	bool ja_sce_1;
	public GameObject ter_scenario_2;
	bool ja_sce_2;
	
	//INIMIGO
	public GameObject ter_enemy_1;
	bool ja_ene_1;
	public GameObject ter_enemy_2;
	bool ja_ene_2;
	public GameObject ter_enemy_3;
	bool ja_ene_3;
	public GameObject ter_enemy_4;
	bool ja_ene_4;
	
	//CENARIO E MOEDA
	public GameObject ter_sce_coi_1;
	bool ja_sce_coi_1;
	public GameObject ter_sce_coi_2;
	bool ja_sce_coi_2;
	public GameObject ter_sce_coi_3;
	bool ja_sce_coi_3;
	public GameObject ter_sce_coi_4;
	bool ja_sce_coi_4;
	

	//INIMIGO E MOEDA
	public GameObject ter_ene_coi_1;
	bool ja_ene_coi_1;
	public GameObject ter_ene_coi_2;
	bool ja_ene_coi_2;
	public GameObject ter_ene_coi_3;
	bool ja_ene_coi_3;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public GameObject getTerrain(int number) {
		GameObject res;
		if(number == 1 || number == 5) {
			res = createCoin();
		} else if(number == 4 || number == 9) {
			res = createScenario();
		} else if(number == 2 || number == 6 || number == 10 || number == 13) {
			res = createEnemy();
		} else if(number == 8 || number == 12 || number == 15) {
			res = createEnemyCoin();
		} else {
			res = createScenarioCoin();
		}
		return res;
	}
	
	
	GameObject createCoin() {
		GameObject res = null;
		int ran = 0;
		bool jaCriou = true;
		while(jaCriou) {
			ran = Random.Range(0,2);
			switch(ran) {
			case 0:
				res = ter_moeda_1;
				jaCriou = ja_moeda_1;
				break;
			case 1:
				res = ter_moeda_2;
				jaCriou = ja_moeda_2;
				break;
			}
		}
		switch(ran) {
		case 0:
			ja_moeda_1 = true;
			break;
		case 1:
			ja_moeda_2 = true;
			break;
		}
		return res;
	}
	
	
	GameObject createScenario() {
		GameObject res = null;
		int ran = 0;
		bool jaCriou = true;
		while(jaCriou) {
			ran = Random.Range(0,2);
			switch(ran) {
			case 0:
				res = ter_scenario_1;
				jaCriou = ja_sce_1;
				break;
			case 1:
				res = ter_scenario_2;
				jaCriou = ja_sce_2;
				break;
			}
		}
		switch(ran) {
		case 0:
			ja_sce_1 = true;
			break;
		case 1:
			ja_sce_2 = true;
			break;
		}
		return res;
	}
	
	
	GameObject createEnemy() {
		GameObject res = null;
		int ran = 0;
		bool jaCriou = true;
		while(jaCriou) {
			ran = Random.Range(0,4);
			switch(ran) {
			case 0:
				res = ter_enemy_1;
				jaCriou = ja_ene_1;
				break;
			case 1:
				res = ter_enemy_2;
				jaCriou = ja_ene_2;
				break;
			case 2:
				res = ter_enemy_3;
				jaCriou = ja_ene_3;
				break;
			case 3:
				res = ter_enemy_4;
				jaCriou = ja_ene_4;
				break;
			}
		}
		switch(ran) {
		case 0:
			ja_ene_1 = true;
			break;
		case 1:
			ja_ene_2 = true;
			break;
		case 2:
			ja_ene_3 = true;
			break;
		case 3:
			ja_ene_4 = true;
			break;
		}
		return res;
	}
	
	
	GameObject createScenarioCoin() {
		GameObject res = null;
		int ran = 0;
		bool jaCriou = true;
		while(jaCriou) {
			ran = Random.Range(0,4);
			switch(ran) {
			case 0:
				res = ter_sce_coi_1;
				jaCriou = ja_sce_coi_1;
				break;
			case 1:
				res = ter_sce_coi_2;
				jaCriou = ja_sce_coi_2;
				break;
			case 2:
				res = ter_sce_coi_3;
				jaCriou = ja_sce_coi_3;
				break;
			case 3:
				res = ter_sce_coi_4;
				jaCriou = ja_sce_coi_4;
				break;
			}
		}
		switch(ran) {
		case 0:
			ja_sce_coi_1 = true;
			break;
		case 1:
			ja_sce_coi_2 = true;
			break;
		case 2:
			ja_sce_coi_3 = true;
			break;
		case 3:
			ja_sce_coi_4 = true;
			break;
		}
		return res;
	}


	GameObject createEnemyCoin() {
		GameObject res = null;
		int ran = 0;
		bool jaCriou = true;
		while(jaCriou) {
			ran = Random.Range(0,3);
			switch(ran) {
			case 0:
				res = ter_ene_coi_1;
				jaCriou = ja_ene_coi_1;
				break;
			case 1:
				res = ter_ene_coi_2;
				jaCriou = ja_ene_coi_2;
				break;
			case 2:
				res = ter_ene_coi_3;
				jaCriou = ja_ene_coi_3;
				break;
			}
		}
		switch(ran) {
		case 0:
			ja_ene_coi_1 = true;
			break;
		case 1:
			ja_ene_coi_2 = true;
			break;
		case 2:
			ja_ene_coi_3 = true;
			break;
		}
		return res;
	}
}
