﻿using UnityEngine;
using System.Collections;

public class unicorn_terrain_spawner_script : MonoBehaviour {

	public int numberLastCreated;
	public GameObject lastCreated;

	public GameObject terrain_reto;
	public GameObject terrain_alto;
	public GameObject terrain_baixo;
	public GameObject terrain_ilha;
	public GameObject terrain_alto_conection;
	public GameObject terrain_baixo_conection;

	public GameObject terrain_baixo_continue;
	public GameObject terrain_alto_continue;

	// Use this for initialization
	void Start () {
		lastCreated = (GameObject)Instantiate(terrain_reto,Vector3.zero,Quaternion.identity);
	}
	
	// Update is called once per frame
	void Update () {
		if(lastCreated.transform.position.x > 0) {


			if(numberLastCreated == 4) {
				lastCreated = (GameObject)Instantiate(terrain_baixo_continue,new Vector3(lastCreated.transform.position.x - 30, lastCreated.transform.position.y, lastCreated.transform.position.z),Quaternion.identity);
				numberLastCreated = 6;
			} else if(numberLastCreated == 5) {
				lastCreated = (GameObject)Instantiate(terrain_alto_continue,new Vector3(lastCreated.transform.position.x - 30, lastCreated.transform.position.y, lastCreated.transform.position.z),Quaternion.identity);
				numberLastCreated = 7;
			} else {
				int ran = Random.Range(0,6);
				numberLastCreated = ran;
				GameObject clone;

				switch(ran) {
				case 0:
					clone = (GameObject)Instantiate(terrain_reto,new Vector3(lastCreated.transform.position.x - 30, lastCreated.transform.position.y, lastCreated.transform.position.z),Quaternion.identity);
					lastCreated = clone;
					break;
				case 1:
					clone = (GameObject)Instantiate(terrain_alto,new Vector3(lastCreated.transform.position.x - 30, lastCreated.transform.position.y, lastCreated.transform.position.z),Quaternion.identity);
					lastCreated = clone;
					break;
				case 2:
					clone = (GameObject)Instantiate(terrain_baixo,new Vector3(lastCreated.transform.position.x - 30, lastCreated.transform.position.y, lastCreated.transform.position.z),Quaternion.identity);
					lastCreated = clone;
					break;
				case 3:
					clone = (GameObject)Instantiate(terrain_ilha,new Vector3(lastCreated.transform.position.x - 30, lastCreated.transform.position.y, lastCreated.transform.position.z),Quaternion.identity);
					lastCreated = clone;
					break;
				case 4:
					clone = (GameObject)Instantiate(terrain_alto_conection,new Vector3(lastCreated.transform.position.x - 30, lastCreated.transform.position.y, lastCreated.transform.position.z),Quaternion.identity);
					lastCreated = clone;
					break;
				case 5:
					clone = (GameObject)Instantiate(terrain_baixo_conection,new Vector3(lastCreated.transform.position.x - 30, lastCreated.transform.position.y, lastCreated.transform.position.z),Quaternion.identity);
					lastCreated = clone;
					break;
				}
			}
		}
	}
}
