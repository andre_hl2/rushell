﻿using UnityEngine;
using System.Collections;

public class pass_level_script : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if(coll.gameObject.tag == "Player") {
			string name = "";
			if(Game.Data.NameOFLevel == "cena_temple") {
				name = "levelsClearTemple";
			} else if(Game.Data.NameOFLevel == "cena_unicorn") {
				name = "levelsClearUnicorn";
			} else if(Game.Data.NameOFLevel == "cena_canabalt") {
				name = "levelsClearCanabalt";
			}
			int alreadyCleared = PlayerPrefs.GetInt(name);
			if(alreadyCleared < Game.Data.actualLevel) {
				PlayerPrefs.SetInt(name,Game.Data.actualLevel);
				PlayerPrefs.Save();
			}
			Game.Data.levelTerminated = true;
		}
	}
}
