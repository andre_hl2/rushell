﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class terrain_spawner_script : MonoBehaviour {

	public int levelcap;
	public GameObject lastCreated;

	public GameObject initial_terrain;
	public GameObject end_level_terrain;

	public int numberOfCreated = 0;

	public GameObject difficult_1_holder;
	public GameObject difficult_2_holder;
	public GameObject difficult_3_holder;
	public GameObject difficult_4_holder;
	public GameObject difficult_5_holder;
	public GameObject difficult_6_holder;

	public int difficult;
	public int numberOfLevel;

	public GameObject[] sortedBlocks;
	public int total_coins;
	public int total_enemies;

	// Use this for initialization
	void Start () {
		//lastCreated = (GameObject)Instantiate(initial_terrain,new Vector3(0,0,0), Quaternion.identity);
		numberOfCreated = 1;

		if(PlayerPrefs.HasKey("difficult")) {
			difficult = PlayerPrefs.GetInt("difficult");
		} else {
			difficult = 1;
		}
		if(PlayerPrefs.HasKey("numberOfLevel")) {
			numberOfLevel = PlayerPrefs.GetInt("numberOfLevel");
		} else {
			numberOfLevel = 1;
		}
		Game.Data.actualLevel = numberOfLevel;


		//SORTEIO DOS BLOCOS
		sortedBlocks = new GameObject[levelcap];

		for(numberOfCreated = 1;numberOfCreated<levelcap;numberOfCreated++) {

			GameObject clone = new GameObject();
			GameObject selected = difficult_1_holder;
			
			switch(difficult) {
			case 1:
				selected = difficult_1_holder;
				break;
			case 2:
				selected = difficult_2_holder;
				break;
			case 3:
				selected = difficult_3_holder;
				break;
			case 4:
				selected = difficult_4_holder;
				break;
			case 5:
				selected = difficult_5_holder;
				break;
			case 6:
				selected = difficult_6_holder;
				break;
			}
			
			//ALGORITMO DE CRIACAO
			if(numberOfCreated == 1) {
				clone = selected.GetComponent<Difficult_terrain_manager_script>().getTerrainCoin();
			} else 
			if(numberOfCreated == 4 || numberOfCreated == 8) {
				clone = selected.GetComponent<Difficult_terrain_manager_script>().getTerrainScenario();
			} else
			if(numberOfCreated == 2 || numberOfCreated == 6) {
				clone = selected.GetComponent<Difficult_terrain_manager_script>().getTerrainEnemy();
			} else
			if(numberOfCreated == 3 || numberOfCreated == 7) {
				clone = selected.GetComponent<Difficult_terrain_manager_script>().getTerrainCoiScenario();
			} else
			if(numberOfCreated == 5 || numberOfCreated == 9) {
				clone = selected.GetComponent<Difficult_terrain_manager_script>().getTerrainCoiEnemy();
			} else
			if(numberOfCreated == 10) {
				clone  = selected.GetComponent<Difficult_terrain_manager_script>().getTerrainSceEnemy();
			}

			count_coins_enemies_script counter = clone.GetComponent<count_coins_enemies_script>();
			if(counter != null) {
				total_coins += counter.coin_number;
				total_enemies += counter.enemy_number;
			}

			sortedBlocks[numberOfCreated] = clone;
		}

		numberOfCreated = 1;
		Stars_Rank_Script.Data.desiredCoins = (int)Mathf.Floor( total_coins * 0.8f );
		Stars_Rank_Script.Data.desiredEnemies = (int)Mathf.Floor( total_enemies / 3);
		if(Stars_Rank_Script.Data.desiredEnemies < 1)
			Stars_Rank_Script.Data.desiredEnemies = 1;
		Stars_Rank_Script.Data.roundEnemies = 0;
		Stars_Rank_Script.Data.GoldBarrelsExploded = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if(lastCreated.transform.position.x > 0) {

			if(numberOfCreated < levelcap) {
				lastCreated = (GameObject)Instantiate(sortedBlocks[numberOfCreated], new Vector3(lastCreated.transform.position.x - 30, lastCreated.transform.position.y, lastCreated.transform.position.z), Quaternion.identity);
				numberOfCreated++;
			} else {
				GameObject clone = (GameObject)Instantiate(end_level_terrain,new Vector3(lastCreated.transform.position.x - 30, lastCreated.transform.position.y, lastCreated.transform.position.z),Quaternion.identity);
				lastCreated = clone;
			}
		}
	}
}
