﻿using UnityEngine;
using System.Collections;

public class Spawn_Manager_script : MonoBehaviour {

	public GameObject lastCreated;

	//temporario
	public GameObject terrain;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(lastCreated.transform.position.x > 30) {
			GameObject clone = (GameObject)Instantiate(terrain,new Vector3(lastCreated.transform.position.x - 60, lastCreated.transform.position.y, lastCreated.transform.position.z),Quaternion.identity);
			Destroy(lastCreated, 3);
			lastCreated = clone;
		}
	}
}
