﻿using UnityEngine;
using System.Collections;


public class Difficult_terrain_manager_script : MonoBehaviour {

	public int difficult;

	public GameObject[] terrain;
	public bool[] alreadyCreated;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public GameObject getTerrainCoin() {
		return drawTerrain(0);
	}

	public GameObject getTerrainScenario() {
		return drawTerrain(5);
	}

	public GameObject getTerrainEnemy() {
		return drawTerrain(10);
	}

	public GameObject getTerrainCoiScenario() {
		return drawTerrain(15);
	}

	public GameObject getTerrainCoiEnemy() {
		return drawTerrain(20);
	}

	public GameObject getTerrainSceEnemy() {
		return drawTerrain(25);
	}

	public GameObject drawTerrain(int first) {
		int ran = Random.Range(0,5);

		while(true) {
			if(alreadyCreated[first + ran]) {
				ran = Random.Range(0,5);
			} else {
				alreadyCreated[first + ran] = true;
				return terrain[first + ran];
			}
		}
	}

}
