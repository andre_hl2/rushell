﻿using UnityEngine;
using System.Collections;

public class stop_camera_pass_level_script : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if(coll.gameObject.tag == "Player") {
			Game.Data.cameraFollowing = false;
			Game.Data.passedLevel = true;
		}
	}
}
