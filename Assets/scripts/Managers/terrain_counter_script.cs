﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(count_coins_enemies_script))]
public class terrain_counter_script : Editor {
	public override void OnInspectorGUI() {
		DrawDefaultInspector();

		count_coins_enemies_script myScript = (count_coins_enemies_script)target;

		if(GUILayout.Button("Contar moedas e inimigos")) {
			myScript.Count();
		}
	}
}
#endif
