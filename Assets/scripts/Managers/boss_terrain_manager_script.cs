﻿using UnityEngine;
using System.Collections;

public class boss_terrain_manager_script : MonoBehaviour {

	public GameObject terrain_normal;
	public GameObject terrain_hard;
	public GameObject terrain_end_level;

	GameObject lastCreated;

	// Use this for initialization
	void Start () {
		if(Game.Data.actualLevel == 10) {
			lastCreated = (GameObject)Instantiate(terrain_hard, Vector3.zero, Quaternion.identity);
		} else {
			lastCreated = (GameObject)Instantiate(terrain_normal, Vector3.zero, Quaternion.identity);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(lastCreated.transform.position.x > 0) {
			if(Game.Data.deadBoss) {
				GameObject clone = (GameObject)Instantiate(terrain_end_level,new Vector3(lastCreated.transform.position.x - 30, lastCreated.transform.position.y, lastCreated.transform.position.z),Quaternion.identity);
				lastCreated = clone;
			} else {
				if(Game.Data.actualLevel == 10) {
					GameObject clone = (GameObject)Instantiate(terrain_hard,new Vector3(lastCreated.transform.position.x - 30, lastCreated.transform.position.y, lastCreated.transform.position.z),Quaternion.identity);
					lastCreated = clone;
				} else {
					GameObject clone = (GameObject)Instantiate(terrain_normal,new Vector3(lastCreated.transform.position.x - 30, lastCreated.transform.position.y, lastCreated.transform.position.z),Quaternion.identity);
					lastCreated = clone;
				}
			}
		}
	}
}
