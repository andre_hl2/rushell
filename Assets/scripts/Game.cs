﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class Game : MonoBehaviour {
	
	public static Game Data;
	public bool Paused = false;
	public GameObject Flecha;
	public GameObject normal_shoot;
	public GameObject special_shoot_holder;
	public bool cameraFollowing = true;
	public bool onBoss = false;
	public bool controlSpace = false;
	public float HSpeed = 0;
	public RushellData Rushell = new RushellData();
	public string NameOFLevel;
	public int actualLevel;
	public bool passedLevel = false;
	public bool levelTerminated = false;

	//shoot related things
	public bool shooting;
	public bool normal_shooting;
	public bool special_shooting;
	public float str;
	public float angle;
	public bool canSpecial;
	public bool canShoot;
	public float timeToShoot;
	public float actualTimeToShoot;

	//boss things
	public bool deadBoss;

	//BMG related things
	public string LevelMusicName;
	public AudioClip LevelMusic1;
	public AudioClip LevelMusic2;
	public AudioClip LevelMusic3;
	public AudioClip LevelMusic4;

	//help related things
	public bool showingHelp = false;

	//Dead text string
	public string deadText;

	public void Awake() {
		if (Data == null) {
			Data = this;
			
			if(PlayerPrefs.HasKey("coins")) {
				Data.Rushell.coins = PlayerPrefs.GetInt("coins");
			} else {
				Data.Rushell.coins = 0;
			}
			if(PlayerPrefs.HasKey("numberOfLevel")) {
				actualLevel = PlayerPrefs.GetInt("numberOfLevel");
			}
			if(PlayerPrefs.HasKey("nameOfLevel")){
				NameOFLevel = PlayerPrefs.GetString("nameOfLevel");
			}
			Time.timeScale = 1;

			if(SoundController.Data != null) {
				if(!SoundController.Data.mutedBMG) {
					if(SoundController.Data.actualBMG != LevelMusicName) {

						if(SoundController.Data.BMGAudioSource1 != null)
							SoundController.Data.BMGAudioSource1.Pause();
						if(SoundController.Data.BMGAudioSource2 != null)
							SoundController.Data.BMGAudioSource2.Pause();
						if(SoundController.Data.BMGAudioSource3 != null)
							SoundController.Data.BMGAudioSource3.Pause();
						if(SoundController.Data.BMGAudioSource4 != null)
							SoundController.Data.BMGAudioSource4.Pause();

						if(LevelMusic1 != null) {
							SoundController.Data.BMGAudioSource1.clip = LevelMusic1;
							SoundController.Data.BMGAudioSource1.loop = true;
							SoundController.Data.BMGAudioSource1.Play();
						}
						if(LevelMusic2 != null) {
							SoundController.Data.BMGAudioSource2.clip = LevelMusic2;
							SoundController.Data.BMGAudioSource2.loop = true;
							SoundController.Data.BMGAudioSource2.Play();
						}
						if(LevelMusic3 != null) {
							SoundController.Data.BMGAudioSource3.clip = LevelMusic3;
							SoundController.Data.BMGAudioSource3.loop = true;
							SoundController.Data.BMGAudioSource3.Play();
						}
						if(LevelMusic4 != null) {
							SoundController.Data.BMGAudioSource4.clip = LevelMusic4;
							SoundController.Data.BMGAudioSource4.loop = true;
							SoundController.Data.BMGAudioSource4.Play();
						}

						SoundController.Data.actualBMG = LevelMusicName;
					}
				}
			}

			// DEAD TEXT 
			int ran = Random.Range(0,13);
			switch(ran) {
			case 0:
				deadText = "R.I.P. (Rushell in Pieces)";
				break;
			case 1:
				deadText = "Rushell is no more...";
				break;
			case 2:
				deadText = "The Carrot is a Lie";
				break;
			case 3:
				deadText = "That's all folks...";
				break;
			case 4:
				deadText = "You Died";
				break;
			case 5:
				deadText = "May the Carrot be with you";
				break;
			case 6:
				deadText = "Would you kindly try again";
				break;
			case 7:
				deadText = "Combo Breaker!!!";
				break;
			case 8:
				deadText = "All your carrot are belong to us";
				break;
			case 9:
				deadText = "Rushell?? RUSHEEEEEEEEEL???";
				break;
			case 10:
				deadText = "Wasted";
				break;
			case 11:
				deadText = "Die, Dies, Died";
				break;
			case 12:
				deadText = "Go home, be a family rabbit";
				break;
			}

		}
	}

	void Update() {
		if(actualTimeToShoot >= 0)
			actualTimeToShoot -= Time.deltaTime;
		else
			canShoot = true;

		if(Tutorial.Data != null) {
			Game.Data.Rushell.Animator.enabled = (!Game.Data.Paused && !Tutorial.Data.showing);
		} else {
			Game.Data.Rushell.Animator.enabled = !Game.Data.Paused;
		}

		AudioLowPassFilter low = Camera.main.GetComponent<AudioLowPassFilter>();
		if(low != null) {
			if(Game.Data.Paused) {
				if(low.cutoffFrequency > 1400)
					low.cutoffFrequency -= 500;
			} else {
				if(low.cutoffFrequency < 22000) {
					low.cutoffFrequency += 500;
				}
			}
		}
	}
	
    [System.Serializable]
	public class RushellData {
		public GameObject Root;
		public GameObject Bazooka;
		private Rigidbody2D _rigidbody;
		private Animator _animator;
		private Power_ups_script _powerUp;
		public bool rolling = false;
		public bool pounding = false;
		public bool floating = false;
		public bool invencible = false;
		public bool canJump = true;
		public bool dead = false;
		
		public Rigidbody2D RigidBody {
			get {
				if (_rigidbody == null && Root != null)
					_rigidbody = Root.GetComponent<Rigidbody2D>();
				return _rigidbody;
			}
		}

		public Animator Animator {
			get {
				if (_animator == null && Root != null)
					_animator = Root.GetComponent<Animator>();
				return _animator;
			}
		}

		public Power_ups_script PowerUps {
			get {
				if(_powerUp == null && Root != null)
					_powerUp = Root.GetComponent<Power_ups_script>();
				return _powerUp;
			}
		}

		public int coins;
		public int CoinCollectRadius;
	}

	//recebe o gamebject que sera lancado, a posicao de spawn, o angulo e a forca (vc deve balancear a forca e o angulo dai)
	static public GameObject spawnShoot(GameObject model,Vector3 pos,float angle = 0) {
		if(angle == 0)
			angle = Game.Data.angle;

		Vector3 initialSpeed = new Vector3(-Game.Data.str,0,0);
		initialSpeed = Quaternion.Euler(0,0,angle) * initialSpeed;
		GameObject clone = (GameObject)Instantiate(model,pos,Quaternion.identity);
		//clone.transform.position += new Vector3(-0.7f,0.5f,0);
		clone.GetComponent<Rigidbody2D>().velocity = initialSpeed;
		return clone;
	}
}
