﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Stars_Rank_Script : MonoBehaviour {

	public static Stars_Rank_Script Data;

	public int desiredCoins;
	public int roundCoins;

	public int desiredEnemies;
	public int roundEnemies;

	public int GoldBarrelsExploded;


	public Text txt_enemiesToKill;
	public Text txt_BarrelsToExplode;

	public void Awake() {
		if(Data == null) {
			Data = this;
		}
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(txt_enemiesToKill != null)
			txt_enemiesToKill.text = roundEnemies.ToString()+"/"+desiredEnemies.ToString();
		if(txt_BarrelsToExplode  != null)
			txt_BarrelsToExplode.text = GoldBarrelsExploded.ToString()+"/3";
	}

	public int getStars() {
		int stars = 0;
		if(roundCoins > desiredCoins)
			stars++;
		if(GoldBarrelsExploded == 3)
			stars++;
		if(roundEnemies >= desiredEnemies)
			stars++;

		Debug.Log("Conseguiu: "+stars.ToString()+" estrelas.");

		return stars;
	}
}
