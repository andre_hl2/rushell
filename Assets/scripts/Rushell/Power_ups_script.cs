﻿using UnityEngine;
using UnityEngine.UI;

public class Power_ups_script : MonoBehaviour {

	//SKILLS
	public bool pwu_double_jump = false;
	public bool pwu_float = false;
	public bool pwu_turbo = false;

	public bool pwu_rocket = false;
	public bool pwu_mega_rocket = false;
	public bool pwu_hammer = false;

	public bool pwu_pound = false;
	public bool pwu_mega_pound = false;
	public bool pwu_pogo = false;

	public bool pwu_roll = false;
	public bool pwu_ghost_rool = false;
	public bool pwu_power_rool = false;

	//arma especial
	public bool has_special_weapon;
	public int weapon;
	public int weapon_level;

	public GameObject triple_shoot;
	public GameObject buraco_negro;
	public GameObject tornado;
	public GameObject seq_shoot;
	public GameObject gold_shoot;
	public GameObject firework_shoot;
	public GameObject missile_shoot;

	//IMAGENS
	public Image special_weapon_Image;
	public Sprite sprite_triple_shoot;
	public Sprite sprite_buraco_negro;
	public Sprite sprite_tornado;
	public Sprite sprite_seq_shoot;
	public Sprite sprite_gold_shoot;
	public Sprite sprite_firework_shoot;
	public Sprite sprite_missile_shoot;

	// Use this for initialization
	void Start () {

		/*---------------------------*/
		/*                           */
		/*         HABILIDADES       */
		/*                           */
		/*---------------------------*/
		if(PlayerPrefs.HasKey("pwu_double_tap")) {
			int double_tap_res = PlayerPrefs.GetInt("pwu_double_tap");
			pwu_double_jump = (double_tap_res == 1);
			pwu_float = (double_tap_res == 2);
			pwu_turbo = (double_tap_res == 3);
		}
		if(PlayerPrefs.HasKey("pwu_tap_shoot")) {
			int tap_shoot_res = PlayerPrefs.GetInt("pwu_tap_shoot");
			pwu_rocket = (tap_shoot_res == 1);
			pwu_mega_rocket = (tap_shoot_res == 2);
			pwu_hammer = (tap_shoot_res == 3);
		}
		if(PlayerPrefs.HasKey("pwu_tap_swipe")) {
			int tap_swipe = PlayerPrefs.GetInt("pwu_tap_swipe");
			pwu_pound = (tap_swipe == 1);
			pwu_mega_pound = (tap_swipe == 2);
			pwu_pogo = (tap_swipe == 3);
		}
		if(PlayerPrefs.HasKey("pwu_swipe")) {
			int swipe_res = PlayerPrefs.GetInt("pwu_swipe");
			pwu_roll = (swipe_res == 1);
			pwu_ghost_rool = (swipe_res == 2);
			pwu_power_rool = (swipe_res == 3);
		}

		/*---------------------------*/
		/*                           */
		/*      ARMAS ESPECIAIS      */
		/*                           */
		/*---------------------------*/
	
		if(PlayerPrefs.HasKey("hasSpecialWeapon")) {
			has_special_weapon = (PlayerPrefs.GetInt("hasSpecialWeapon") == 1);
		} else {
			has_special_weapon = false;
		}

		if(has_special_weapon) {
			if(PlayerPrefs.HasKey("specialWeapon")) {
				weapon = PlayerPrefs.GetInt("specialWeapon");
			} else {
				has_special_weapon = false;
				weapon = 0;
			}
		}

		switch(weapon) {
		case 1:
			special_weapon_Image.sprite = sprite_triple_shoot;
			special_weapon_Image.color = Color.white;
			weapon_level = PlayerPrefs.GetInt("triple_shot_level");
			break;
		case 2:
			special_weapon_Image.sprite = sprite_buraco_negro;
			special_weapon_Image.color = Color.white;
			weapon_level = PlayerPrefs.GetInt("buraco_negro_level");
			break;
		case 3:
			special_weapon_Image.sprite = sprite_tornado;
			special_weapon_Image.color = Color.white;
			weapon_level = PlayerPrefs.GetInt("tornado_level");
			break;
		case 4:
			special_weapon_Image.sprite = sprite_seq_shoot;
			special_weapon_Image.color = Color.white;
			weapon_level = PlayerPrefs.GetInt("seq_shot_level");
			break;
		case 5:
			special_weapon_Image.sprite = sprite_gold_shoot;
			special_weapon_Image.color = Color.white;
			weapon_level = PlayerPrefs.GetInt("gold_shot_level");
			break;
		case 6:
			special_weapon_Image.sprite = sprite_firework_shoot;
			special_weapon_Image.color = Color.white;
			weapon_level = PlayerPrefs.GetInt("firework_shot_level");
			break;
		case 7:
			special_weapon_Image.sprite = sprite_missile_shoot;
			special_weapon_Image.color = Color.white;
			weapon_level = PlayerPrefs.GetInt("missile_shot_level");
			break;
		default:
			break;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void shootSpecial() {
		GameObject clone;
		switch(weapon) {
		case 1:   //TRIPLE SHOOT
			clone = (GameObject)Instantiate(triple_shoot,Vector3.zero,Quaternion.identity);
			clone.GetComponent<triple_shoot_script>().level = weapon_level;
			clone.GetComponent<triple_shoot_script>().spawnChilds();
			Destroy (clone.gameObject);
			break;
		case 2:   // BURACO NEGRO
			clone = Game.spawnShoot(buraco_negro, Game.Data.Rushell.Bazooka.transform.position);
			clone.GetComponent<black_hole_script>().level = weapon_level;
			break;
		case 3:	  //TORNADO
			clone = Game.spawnShoot(tornado, Game.Data.Rushell.Bazooka.transform.position);
			clone.GetComponent<tornado_script>().level = weapon_level;
			break;
		case 4:   //TIRO SEQUENCIAL
			clone = (GameObject) Instantiate(seq_shoot, Vector3.zero, Quaternion.identity);
			clone.GetComponent<seq_shoot_script>().level = weapon_level;
			clone.GetComponent<seq_shoot_script>().Shoot();
			break;
		case 5:   // TIRO DE OURO
			clone = Game.spawnShoot(gold_shoot,Game.Data.Rushell.Bazooka.transform.position);
			clone.GetComponent<golden_tiro_script>().level = weapon_level;
			break;
		case 6:
			clone = Game.spawnShoot(firework_shoot,Game.Data.Rushell.Bazooka.transform.position);
			clone.GetComponent<firework_tiro_script>().level = weapon_level;
			break;
		case 7:
			clone = (GameObject) Instantiate(missile_shoot, Vector3.zero, Quaternion.identity);
			clone.GetComponent<homing_shot_script>().level = weapon_level;
			clone.GetComponent<homing_shot_script>().spawnChilds();
			Destroy(clone.gameObject);
			break;
		}
	}
}
