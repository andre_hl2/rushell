﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class predict_script : MonoBehaviour {

	public int numberOfDots;
	public GameObject dotPrefab;
	public List<GameObject> dots;

	public bool aiming = false;

	// Use this for initialization
	void Start () {
		dots = new List<GameObject>();
		for(int i=0;i<numberOfDots;i++) {
			GameObject dot = (GameObject) Instantiate(dotPrefab);
			dots.Insert(i, dot);
		}
		stopAim();
	}
	
	// Update is called once per frame
	void Update () {
		if(!aiming) {
			if(Game.Data.shooting) {
				beginAim();
			}
		} else {
			if(!Game.Data.shooting) {
				stopAim();
			}
		}

		if(aiming) {
			Vector2 initialVel = new Vector3(-Game.Data.str,0);
			initialVel = Quaternion.Euler(0,0,Game.Data.angle) * initialVel;
			Vector2 initialPos = Game.Data.Rushell.Bazooka.transform.position;

			for (int i =0; i < numberOfDots; i++) 
			{
				//antes os tres valores estavam como 0.020f, a massa do tiro era 1, a forca era 23, e a gravity scale 1

				initialVel += Physics2D.gravity * 0.030f;
				
				initialPos += initialVel * 0.010f;
				//initialPos.x -= Game.Data.HSpeed * 0.002f;
				
				dots[i].transform.position = initialPos;
				dots[i].GetComponent<Renderer>().enabled = true;
			}
		}
	}

	void beginAim() {
		aiming = true;
		for(int i=0;i<numberOfDots;i++){
			dots[i].transform.position = Game.Data.Rushell.Root.transform.position;
			float alpha = numberOfDots/(i+1f);
			dots[i].GetComponent<SpriteRenderer>().color = new Color(1,1,1,(alpha/numberOfDots) + 0.3f);
		}
	}

	void stopAim() {
		aiming = false;
		for(int i=0;i<numberOfDots;i++) {
			dots[i].GetComponent<SpriteRenderer>().color = new Color(0,0,0,0);
		}
	}
}
