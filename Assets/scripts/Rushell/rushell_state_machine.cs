using UnityEngine;
using System.Collections;

public class rushell_state_machine : MonoBehaviour {

	public LayerMask groundLayer;

	public GameObject groundCheck;
	public float distToGround;

	public float limitRight;
	public float limitLeft;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if((Game.Data.Rushell.dead) && (Game.Data.Rushell.Animator.GetInteger("Death_Type") == 1)) {
			distToGround = 0.29f;
		} else {
			distToGround = transform.position.y - groundCheck.transform.position.y;
		}

		int ground = isGrounded();
		Game.Data.Rushell.Animator.SetInteger("Grounded", ground);
		if(ground == 0 && !Game.Data.Rushell.floating && !Game.Data.Paused) {
			Game.Data.Rushell.RigidBody.AddForce(Physics2D.gravity);
		}

		if(Game.Data.onBoss) {
			Vector3 pos = Game.Data.Rushell.Root.transform.position;
			if(pos.x > limitRight) {
				pos.x = limitRight;
				Game.Data.Rushell.Root.transform.position = pos;
			} else if(pos.x < limitLeft) {
				pos.x = limitLeft;
				Game.Data.Rushell.Root.transform.position = pos;
			}

		} else {
			if(Game.Data.controlSpace) {
				Vector3 pos = Game.Data.Rushell.Root.transform.position;
				if(pos.x > limitRight) {
					pos.x = limitRight;
					Game.Data.Rushell.Root.transform.position = pos;
				} else if(pos.x < limitLeft) {
					pos.x = limitLeft;
					Game.Data.Rushell.Root.transform.position = pos;
				}
			}
			if(Game.Data.Rushell.Root.transform.position.x < 5 && Game.Data.cameraFollowing) {
				Vector2 pos = Game.Data.Rushell.Root.transform.position;
				pos.x +=  3f * (Game.Data.HSpeed/8f) * Time.deltaTime;
				Game.Data.Rushell.Root.transform.position = pos;
			}
			if(Game.Data.passedLevel) {
				Vector2 pos = Game.Data.Rushell.Root.transform.position;
				pos.x -=  8f * (Game.Data.HSpeed/8f) * Time.deltaTime;
				Game.Data.Rushell.Root.transform.position = pos;
			}
		}

		//linha de tamanho de coin collect
		Debug.DrawLine(new Vector3(this.transform.position.x + Game.Data.Rushell.CoinCollectRadius, this.transform.position.y,0), new Vector3(this.transform.position.x - Game.Data.Rushell.CoinCollectRadius, this.transform.position.y,0));

	}

	int isGrounded() {
		if(Game.Data.Rushell.RigidBody.velocity.y <= 0) {
			RaycastHit2D hit = Physics2D.Raycast(this.transform.position, new Vector2(0,-1), distToGround, groundLayer);
			if(hit.collider != null) {
				if((hit.point.y < this.transform.position.y - 0.5f) || Game.Data.Rushell.Animator.GetInteger("Death_Type") == 1) {
					this.transform.position = new Vector2(this.transform.position.x, hit.point.y + distToGround);
					Game.Data.Rushell.RigidBody.velocity = new Vector2(0,0);
					Debug.DrawLine(this.transform.position, groundCheck.transform.position,Color.red);
					if(hit.collider.gameObject.tag == "ground") {
						return 1;
					} else if(hit.collider.gameObject.tag == "plataforma") {
						if(Game.Data.Rushell.pounding) {
							breakable_plataform_script bre = hit.collider.gameObject.GetComponent<breakable_plataform_script>();
							if(bre != null) {
								bre.Break();
							} else {
								return 1;
							}
						} else {
							return 2;
						}
					}
				} else {
					Debug.DrawLine(this.transform.position, groundCheck.transform.position,Color.green);
					return 0;
				}
			} else {
				Debug.DrawLine(this.transform.position, groundCheck.transform.position,Color.green);
				return 0;
			}
		}
		return 0;
	}
}
