﻿using UnityEngine;
using System.Collections;

public class shadow_rushell_Script : MonoBehaviour {

	public SpriteRenderer renderer;

	// Use this for initialization
	void Start () {
		renderer = this.gameObject.GetComponent<SpriteRenderer>();
		Color col = renderer.material.color;
		col.a = 0.3f;
		col.r = 1;
		col.b = 1;
		col.g = 1;
		renderer.material.color = col;
		renderer.sprite = Game.Data.Rushell.Root.GetComponent<SpriteRenderer>().sprite;
	}
	
	// Update is called once per frame
	void Update () {
		Color col = renderer.material.color;
		col.a -= 0.01f;
		renderer.material.color = col;
		if(col.a < 0) {
			Destroy(this.gameObject);
		}
	}
}
