﻿using UnityEngine;
using System.Collections;

public class breakable_plataform_script : MonoBehaviour {

	public GameObject break1;
	public GameObject break2;
	public GameObject break3;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Break() {
		Vector3 pos = this.transform.position;
		Instantiate(break1,new Vector3(pos.x-0.8f,pos.y,pos.z),Quaternion.identity);
		Instantiate(break2,pos,Quaternion.identity);
		Instantiate(break3,new Vector3(pos.x+1.04f,pos.y,pos.z),Quaternion.identity);
		Destroy(this.gameObject);
	}
}
