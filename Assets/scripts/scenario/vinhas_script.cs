﻿using UnityEngine;
using System.Collections;

public class vinhas_script : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if(coll.gameObject.tag == "Player") {
			Game.Data.Rushell.canJump = false;
			Game.Data.HSpeed = 4f;
		}
	}

	void OnTriggerStay2D(Collider2D coll) {
		if(coll.gameObject.tag == "Player") {
			Game.Data.Rushell.canJump = false;
			Game.Data.HSpeed = 4f;
		}
	}

	void OnTriggerExit2D(Collider2D coll) {
		if(coll.gameObject.tag == "Player") {
			Game.Data.Rushell.canJump = true;
			Game.Data.HSpeed = 8;
		}
	}

}
