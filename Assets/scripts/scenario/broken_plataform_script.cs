﻿using UnityEngine;
using System.Collections;

public class broken_plataform_script : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Rigidbody2D rig = this.gameObject.GetComponent<Rigidbody2D>();
		if(rig != null) {
			rig.AddTorque(Random.Range(50,100));
			Vector2 initialVel = new Vector3(Random.Range(300,800),0);
			initialVel = Quaternion.Euler(0,0,Random.Range(110,170)) * initialVel; 
			rig.AddForce(initialVel);
		}
	}
	
	// Update is called once per frame
	void Update () {
		SpriteRenderer ren = this.gameObject.GetComponent<SpriteRenderer>();
		if(ren != null) {
			Color col = ren.material.color;

			col.a -= 0.01f;

			if(col.a < 0)
				Destroy(this.gameObject);

			ren.material.color = col;
		}
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if(coll.gameObject.tag == "plataforma" || coll.gameObject.tag == "ground") {
			Vector2 vel = this.gameObject.GetComponent<Rigidbody2D>().velocity;
			vel.y *= -0.5f;
			if(Mathf.Abs(vel.y) < 0.01f) {
				vel = new Vector2(0,0);
			}
			this.gameObject.GetComponent<Rigidbody2D>().velocity = vel;
		}
	}
}
