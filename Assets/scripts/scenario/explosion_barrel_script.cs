﻿using UnityEngine;
using System.Collections;

public class explosion_barrel_script : MonoBehaviour {

	public float distanceHit;
	public GameObject explosionEffect;


	public bool grounded;
	public LayerMask groundLayer;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		grounded = isGrouded();
		if(!grounded) {
			this.gameObject.GetComponent<Rigidbody2D>().AddForce(Physics2D.gravity);
		}
	}

	bool isGrouded() {
		RaycastHit2D hit = Physics2D.Raycast(this.transform.position, new Vector2(0,-1), 0.74f, groundLayer);
		if(hit.collider != null) {
			this.transform.position = new Vector2(this.transform.position.x, hit.point.y + 0.74f);
			this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
			return true;
		} else {
			return false;
		}
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if(coll.gameObject.tag == "tiro") {
			Explodir();
		}
	}

	public void Explodir() {
		this.gameObject.GetComponent<CircleCollider2D>().enabled = false;
		Instantiate(explosionEffect, this.transform.position, Quaternion.identity);
		Collider2D[] res = Physics2D.OverlapCircleAll(this.gameObject.transform.position, distanceHit);
		foreach (Collider2D cool in res) {
			if(cool.gameObject.tag == "inimigo") {
				Destroy(cool.gameObject);
				//SPAWNS A EXPLOSION HERE
			} else if (cool.gameObject.tag == "plataforma") {
				breakable_plataform_script bre = cool.gameObject.GetComponent<breakable_plataform_script>();
				if(bre != null) {
					bre.Break();
				}
			} else {
				explosion_barrel_script exp = cool.gameObject.GetComponent<explosion_barrel_script>();
				if(exp != null) {
					exp.Explodir();
				}
			}
		}
		Destroy(this.gameObject);
	}
}
